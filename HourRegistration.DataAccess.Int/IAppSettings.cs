﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HourRegistration.DataAccess.Int
{
    public interface IAppSettings
    {
        string XmlDataFolder { get; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HourRegistration.DataAccess.Int
{
    public interface IDataAccessor<T>
    {
        List<T> RetrieveListOfEntities();

        List<T> RetrieveEntityListByCode(string code);

        T RetrieveSingleEntity(string code);

        T RetrieveSingleEntityByParentCode(string parentCode, string code);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace HourRegistration.DataEntities
{
    public class Project
    {
        public string Code { get; set; }

        public string Description { get; set; }
    }
}

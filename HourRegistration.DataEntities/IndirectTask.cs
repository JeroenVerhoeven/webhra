﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace HourRegistration.DataEntities
{
    public class IndirectTask
    {
        public int Task { get; set; }

        public string Description { get; set; }
    }
}

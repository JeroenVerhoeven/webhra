﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace HourRegistration.DataEntities
{
    public class ProjectActivity
    {
        public string ProjectCode { get; set; }

        public string ActivityCode { get; set; }

        public string Description { get; set; }
    }
}

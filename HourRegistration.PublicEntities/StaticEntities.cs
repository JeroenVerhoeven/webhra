﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HourRegistration.PublicEntities
{
    public static class StaticEntities
    {
        public static Dictionary<int, string> HourTypes()
        {
            Dictionary<int, string> projectTypes = new Dictionary<int, string>();
            projectTypes.Add(0, "-Kies type-");
            projectTypes.Add(1, "1-Project");
            projectTypes.Add(2, "2-Productie");
            projectTypes.Add(4, "4-Indirect");

            return projectTypes;
        }
    }
}

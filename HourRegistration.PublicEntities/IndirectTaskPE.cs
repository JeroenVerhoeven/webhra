﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace HourRegistration.PublicEntities
{
    [XmlRoot("IndirectTask")]
    public class IndirectTaskPE
    {
        public int Task { get; set; }

        public string Description { get; set; }

        [XmlIgnore]
        public string SearchString
        {
            get { return string.Format("{0} {1}", Task, Description); }
        }
    }
}

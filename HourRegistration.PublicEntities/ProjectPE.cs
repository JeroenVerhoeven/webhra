﻿namespace HourRegistration.PublicEntities
{
    using System.Xml.Serialization;

    [XmlRoot("Project")]
    public class ProjectPE
    {
        public string Code { get; set; }

        public string Description { get; set; }

        [XmlIgnore]
        public string SearchString
        {
            get { return string.Format("{0} {1}", Code, Description); }
        }
    }
}

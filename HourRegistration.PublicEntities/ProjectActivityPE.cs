﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace HourRegistration.PublicEntities
{
    [XmlRoot("ProjectActivity")]
    public class ProjectActivityPE
    {
        public string ProjectCode { get; set; }

        public string ActivityCode { get; set; }

        public string Description { get; set; }

        public string ListItem
        {
            get
            {
                return string.Format("{0} {1}", ActivityCode, Description);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace HourRegistration.Utilities
{
    public static class BaseConverter<S, T> where S : class where T : class
    {
        private static List<PropertyInfo> lstPropertiesSrc;
        private static List<PropertyInfo> lstPropertiesTrg;

        public static T ConvertToSingleEntity(S sourceEntity)
        {
            return ConvertEntity<S, T>(sourceEntity);
        }

        public static List<T> ConvertToEntityList(List<S> srcEntities)
        {
            List<T> entities = (List<T>)Activator.CreateInstance(typeof(List<T>));
            T singleEntity;

            foreach (S srcEntity in srcEntities)
            {
                singleEntity = ConvertToSingleEntity(srcEntity);
                if (singleEntity != null)
                {
                    entities.Add(singleEntity);
                }
            }

            return entities;
        }

        private static TRG ConvertEntity<SRC, TRG>(SRC srcEntity)
        {
            PropertyInfo srcProperty;
            if (srcEntity == null)
            {
                return default(TRG);
            }

            TRG entity = (TRG)Activator.CreateInstance(typeof(TRG));

            lstPropertiesSrc = typeof(SRC).GetProperties().ToList();
            lstPropertiesTrg = typeof(TRG).GetProperties().ToList();

            foreach (PropertyInfo property in lstPropertiesTrg)
            {
                srcProperty = (from prop in lstPropertiesSrc
                               where prop.Name == property.Name &&
                                     prop.PropertyType == property.PropertyType
                               select prop).FirstOrDefault();

                if (srcProperty != null)
                {
                    var value = srcProperty.GetValue(srcEntity, null);
                    property.SetValue(entity, value, null);
                }
            }

            return entity;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace HourRegistration.Utilities
{
    public static class DataSerialization
    {
        public static T DeserializeFromXml<T>(XmlDocument xmlDoc)
        {
            T result;
            XmlSerializer ser = new XmlSerializer(typeof(T));
            using (TextReader tr = new StringReader(xmlDoc.InnerXml.ToString()))
            {
                result = (T)ser.Deserialize(tr);
            }

            return result;
        }

        public static T DeserializeFromFile<T>(string xmlFileName)
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(xmlFileName);
            T result;

            XmlSerializer ser = new XmlSerializer(typeof(T));
            using (TextReader tr = new StringReader(xmlDoc.InnerXml.ToString()))
            {
                result = (T)ser.Deserialize(tr);
            }

            return result;
        }
    }
}

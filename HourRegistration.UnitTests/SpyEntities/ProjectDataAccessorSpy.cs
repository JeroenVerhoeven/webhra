﻿using HourRegistration.DataAccess.Int;
using HourRegistration.DataAccess.Xml;
using HourRegistration.DataEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HourRegistration.UnitTests.SpyEntities
{
    public class ProjectDataAccessorSpy : ProjectDataAccessor
    {
        private bool methodCalled;

        public ProjectDataAccessorSpy(IAppSettings appSettings) : base(appSettings)
        {
        }

        public bool MethodCalled
        {
            get { return this.methodCalled; }
        }

        public string TestDetermineDataFile()
        {
            return this.DetermineDataFile();
        }

        public override List<Project> RetrieveListOfEntities()
        {
            this.methodCalled = false;
            return base.RetrieveListOfEntities();
        }

        protected override string DetermineDataFile()
        {
            this.methodCalled = true;
            return base.DetermineDataFile();
        }

        protected override T RetrieveDataFromFile<T>(string fileName)
        {
            return (T)Activator.CreateInstance(typeof(T));
        }
    }
}

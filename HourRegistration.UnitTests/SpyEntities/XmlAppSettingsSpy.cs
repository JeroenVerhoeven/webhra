﻿using HourRegistration.DataAccess.Xml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HourRegistration.UnitTests.SpyEntities
{
    public class XmlAppSettingsSpy : XmlAppSettings
    {
        private bool useDefaultValue;

        public XmlAppSettingsSpy(bool useDefaults)
        {
            this.useDefaultValue = useDefaults;
        }

        public string TestDetermineAppSetting(string keyValue, string defaultValue)
        {
            return this.DetermineAppSetting(keyValue, defaultValue);
        }

        protected override string DetermineAppSettingValue(string keyName)
        {
            return this.useDefaultValue ? TestConstants.DefaultDataFolder : string.Empty;
        }
    }
}

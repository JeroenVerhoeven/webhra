﻿using HourRegistration.DataAccess.Int;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HourRegistration.UnitTests
{
    public class TestAppSettings : IAppSettings
    {
        public string XmlDataFolder
        {
            get
            {
                return TestConstants.DefaultDataFolder;
            }
        }
    }
}

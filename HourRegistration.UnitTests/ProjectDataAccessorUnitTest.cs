﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using HourRegistration.UnitTests.SpyEntities;
using HourRegistration.DataAccess.Int;
using System.IO;
using HourRegistration.DataAccess.Xml;

namespace HourRegistration.UnitTests
{
    [TestClass]
    public class ProjectDataAccessorUnitTest
    {
        [TestMethod]
        public void ProjectDataAccessor_DetermineDataFile_ReturnFileName()
        {
            //// Arrange
            IAppSettings settings = new TestAppSettings();
            ProjectDataAccessorSpy projectAccessor = new ProjectDataAccessorSpy(settings);
            string defaultFileName = Path.Combine(TestConstants.DefaultDataFolder, DAConstants.ProjectFile);

            //// Act
            string testFileName = projectAccessor.TestDetermineDataFile();

            //// Assert
            Assert.AreEqual(defaultFileName, testFileName, "The file name should determined by the configured folder plus 'Projects.xml'");
        }

        [TestMethod]
        public void ProjectDataAccessor_RetrieveListOfEntities_MethodDetermineDataFileCalled()
        {
            //// Arrange
            IAppSettings settings = new TestAppSettings();
            ProjectDataAccessorSpy projectAccessor = new ProjectDataAccessorSpy(settings);

            //// Act
            var entities = projectAccessor.RetrieveListOfEntities();
            bool methodCalled = projectAccessor.MethodCalled;

            //// Assert
            Assert.IsTrue(methodCalled, "Method 'DetermineDataFile' should be called to determine the file to read the entities from");
        }

        [TestMethod]
        public void ProjectDataAccessor_RetrieveSingleEntity_MethodDetermineDataFileCalled()
        {
            //// Arrange
            IAppSettings settings = new TestAppSettings();
            ProjectDataAccessorSpy projectAccessor = new ProjectDataAccessorSpy(settings);
            string projectCode = "key";

            //// Act
            var entities = projectAccessor.RetrieveSingleEntity(projectCode);
            bool methodCalled = projectAccessor.MethodCalled;

            //// Assert
            Assert.IsTrue(methodCalled, "Method 'DetermineDataFile' should be called to determine the file to read the entities from");
        }
    }
}

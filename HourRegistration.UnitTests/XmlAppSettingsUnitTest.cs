﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using HourRegistration.UnitTests.SpyEntities;

namespace HourRegistration.UnitTests
{
    [TestClass]
    public class XmlAppSettingsUnitTest
    {
        [TestMethod]
        public void XmlAppSettings_DetermineAppSetting_EmptyKeyName_ReturnDefaultValue()
        {
            //// Arrange
            XmlAppSettingsSpy appSettings = new XmlAppSettingsSpy(true);
            string keyValue = string.Empty;
            string defaultValue = "DefaultValue";

            //// Act
            string testValue = appSettings.TestDetermineAppSetting(keyValue, defaultValue);

            //// Assert
            Assert.AreEqual(defaultValue, testValue, "In case of empty key value the default value should be returned");
        }

        [TestMethod]
        public void XmlAppSettings_DetermineAppSetting_KeyNameFilled_ConfigurationFilled_ReturnConfiguredValue()
        {
            //// Arrange
            XmlAppSettingsSpy appSettings = new XmlAppSettingsSpy(true);
            string keyValue = "XmlDataFolder";
            string defaultValue = "DefaultValue";

            //// Act
            string testValue = appSettings.TestDetermineAppSetting(keyValue, defaultValue);

            //// Assert
            Assert.AreEqual(TestConstants.DefaultDataFolder, testValue, "The returned value should be taken from the configuration");
        }

        [TestMethod]
        public void XmlAppSettings_DetermineAppSetting_KeyNameFilled_ConfigurationNotFilled_ReturnDefaultValue()
        {
            //// Arrange
            XmlAppSettingsSpy appSettings = new XmlAppSettingsSpy(false);
            string keyValue = "XmlDataFolder";
            string defaultValue = "DefaultValue";

            //// Act
            string testValue = appSettings.TestDetermineAppSetting(keyValue, defaultValue);

            //// Assert
            Assert.AreEqual(defaultValue, testValue, "If the setting is not configured, the default value should be returned");
        }
    }
}

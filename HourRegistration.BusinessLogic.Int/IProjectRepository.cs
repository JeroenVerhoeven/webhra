﻿using HourRegistration.PublicEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HourRegistration.BusinessLogic.Int
{
    public interface IProjectRepository
    {
        List<ProjectPE> RetrieveProjects();

        ProjectPE RetrieveSingleProject(string projectCode);
    }
}

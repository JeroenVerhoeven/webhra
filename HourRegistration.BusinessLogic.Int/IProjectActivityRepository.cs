﻿using HourRegistration.PublicEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HourRegistration.BusinessLogic.Int
{
    public interface IProjectActivityRepository
    {
        List<ProjectActivityPE> RetrieveActivitiesByProject(string projectCode);

        ProjectActivityPE RetrieveSingleProjectActivity(string projectCode, string activityCode);
    }
}

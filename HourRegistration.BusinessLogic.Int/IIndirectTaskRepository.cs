﻿using HourRegistration.PublicEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HourRegistration.BusinessLogic.Int
{
    public interface IIndirectTaskRepository
    {
        List<IndirectTaskPE> RetrieveIndirectTasks();

        IndirectTaskPE RetrieveSingleTask(int taskCode);
    }
}

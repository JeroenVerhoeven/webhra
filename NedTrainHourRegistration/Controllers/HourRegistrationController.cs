﻿namespace NedTrainHourRegistration.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Globalization;
    using System.Linq;
    using System.Web.Mvc;
    using System.Web.Security;
    using NedTrainHourRegistration.Models;
    using NedTrainHourRegistration.NedTrainServiceReference;
    using NedTrainHourRegistration.ViewModels;
    using NedTrainHourRegistration.Infrastructure;
    using HourRegistration.BusinessLogic.Int;
    using HourRegistration.BusinessLogic.Impl;
    using HourRegistration.DataAccess.Xml;
    using PublicEntities = HourRegistration.PublicEntities;
    using System.Web.Script.Serialization;
    using System.Diagnostics;
    using System.Net.Mail;
    using DnsClient;

    public class HourRegistrationController : Controller
    {
        private readonly ERPService_NedTrainSoapClient serviceClient;
        private string employeeId;
        private HoursPerWeekModel currentModel;
        private WeeklyHoursPerEmployeePE recentHours;

        public HourRegistrationController()
        {
            this.serviceClient = new ERPService_NedTrainSoapClient();

            if (System.Web.HttpContext.Current.User.Identity.IsAuthenticated)
            {
                this.employeeId = System.Web.HttpContext.Current.User.Identity.Name.Split('|')[2];
            }
            TempData["sendMessage"] = "";
        }

        [Authorize]
        [HttpPost]
        public JsonResult WeekDayChanged(string currentWeek, string weekday)
        {
            ErpOutputOfDailyTimeSlotPE timeSlot = null;
            HoursPerWeekModel model = (HoursPerWeekModel)HttpContext.Session["HourModel"];
            DateTime firstDateOfWeek = DateTime.ParseExact(currentWeek.Substring(0, 10), "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None);
            DateTime selectedDate = firstDateOfWeek.AddDays(this.GetDayNumber(weekday));

            int userId = (int)HttpContext.Session["UserCode"];
            timeSlot = this.serviceClient.RetrieveDailyTimeSlot(this.employeeId, userId, selectedDate);

            DateTime startTime = new DateTime(selectedDate.Year, selectedDate.Month, selectedDate.Day, 0, 0, 0);
            DateTime stopTime = new DateTime(selectedDate.Year, selectedDate.Month, selectedDate.Day, 23, 59, 59);

            if (timeSlot.Entities != null && timeSlot.Entities.Length > 0)
            {
                startTime = timeSlot.Entities[0].StartTime;
                stopTime = timeSlot.Entities[0].StopTime;
            }

            if (model.LoggedHours != null)
            {
                var recentHours = (from hours in model.LoggedHours
                                   where hours.StartTime.Date == selectedDate.Date
                                   orderby hours.StartTime descending, hours.SequenceNr descending
                                   select hours).FirstOrDefault();

                if (recentHours != null)
                {
                    startTime = recentHours.EndTime;
                }
            }

            var timeslotResult = new { StartTime = string.Format("{0:HH:mm}", startTime), StopTime = string.Format("{0:HH:mm}", stopTime) };
            return this.Json(timeslotResult, "application/json");
        }

        [Route("")]
        public ActionResult Login()
        {
            ViewBag.CompNr = ConfigurationManager.AppSettings["CompNr"] as string;
            if (User.Identity.IsAuthenticated)
            {
                int weekNumber = this.GetIso8601WeekOfYear(DateTime.Now);
                return this.RedirectToAction("Index", new { year = DateTime.Now.Year, weekNumber = weekNumber });
            }
            ViewBag.globalMessage = TempData["sendMessage"];
            ViewBag.results = TempData["returnResult"];
            return this.View();
        }

        [HttpPost]
        [Route("")]
        public ActionResult Login(LoginModel model, string message = "")
        {           
            if (!ModelState.IsValid)
            {
                if (model.CompNr == 0)
                {
                    ViewBag.CompNr = ConfigurationManager.AppSettings["CompNr"] as string;
                }

                return this.View(model);
            }

            ViewBag.results = TempData["returnResult"];
            TempData["wasSuccess"] = "FALSE";
            try
            {
                var result = this.serviceClient.Login(model.LoginCode, model.Password, model.CompNr);

                if (!result.IsAuthenticated)
                {
                    throw new Exception("De opgegeven combinatie van inlogcode en wachtwoord is onjuist");
                }

                if (Session["UserDefaults"] == null)
                {

                    var UserDefaults = new UserDefaults();

                    // Ophalen van defaults van de ingelogde gebruiker
                    var userDefaults = this.serviceClient.RetrieveUserDefaults(model.CompNr + model.LoginCode);

                    var UserDefaultsFromBaan = new JavaScriptSerializer().Serialize(userDefaults);
                    Session.Add("UserDefaultsFromBaan", UserDefaultsFromBaan);

                    string ZoekMethod = userDefaults.Entities[0].Method.ToString();
                    methods method = (methods)Enum.Parse(typeof(methods), ZoekMethod);
                    UserDefaults.method = method;

                    string ZoekInputMethod = userDefaults.Entities[0].InputMethod.ToString();
                    inputMethods InputMethod = (inputMethods)Enum.Parse(typeof(inputMethods), ZoekInputMethod);
                    UserDefaults.inputMethod = InputMethod;

                    string ZoekZoomMethod = userDefaults.Entities[0].ZoomMethod.ToString();
                    zoomMethods zoomMethod = (zoomMethods)Enum.Parse(typeof(zoomMethods), ZoekZoomMethod);
                    UserDefaults.zoomMethod = zoomMethod;

                    UserDefaults.employeeId = model.CompNr + model.LoginCode;
                    Session.Add("UserDefaults", UserDefaults);
                }

                FormsAuthentication.SetAuthCookie($"{result.EmployeeCode}|{result.Name}|{model.CompNr}{model.LoginCode}", false);

                HttpContext.Session["UserLoginCode"] = model.LoginCode;
                HttpContext.Session["UserPassword"] = model.Password;
                HttpContext.Session["UserCode"] = result.EmployeeCode;
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, "De gebruiker kon niet worden gevonden. Controleer de login code en probeer het opnieuw.");
                ViewBag.CompNr = ConfigurationManager.AppSettings["CompNr"] as string;
                return this.View(model);
            }
            int weekNumber = this.GetIso8601WeekOfYear(DateTime.Now);
            return this.RedirectToAction("Index", new { year = DateTime.Now.Year, weekNumber = weekNumber });
        }

        public ActionResult ResetPassword()
        {
            ResetPasswordModel model = new ResetPasswordModel();
            ViewBag.CompNr = ConfigurationManager.AppSettings["CompNr"] as string;

            return this.View(model);
        }

        [HttpPost]
        public ActionResult ResetPassword(ResetPasswordModel model)
        {
            SmtpClient client = new System.Net.Mail.SmtpClient();

            string to = "nedtrain-it-sd-team-reno-haarlem@ns.nl";

            string from = "webhra@ns.nl";

            string server = "mailrelay.ns.nl";

            string poort = "25";

            bool ssl = false;

            MailMessage message = new MailMessage(from, to);

            message.Subject = "Aanvraag nieuw wachtwoord WebHRA";

            message.Body = @"Er is een aanvraag binnen gekomen voor een nieuw wachtwoord voor gebruiker " + model.LoginCode + ". <br> U kunt het nieuwe wachtwoord opsturen naar <a href='mailto:" + model.emailAddress + "'>" + model.emailAddress + "</a>";

            message.Bcc.Add("jeroen.verhoeven@ns.nl");

            message.IsBodyHtml = true;

            client = new System.Net.Mail.SmtpClient
            {
                Host = server,
                Port = Convert.ToInt32(poort),
                EnableSsl = Convert.ToBoolean(ssl),
                DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
            };

            try
            {
                TempData["sendMessage"] = "Uw aanvraag voor een nieuw wachtwoord is verstuurd";
                client.Send(message);
            }
            catch (Exception ex)
            {
                TempData["sendMessage"] = "Exception caught in CreateTestMessage2(): {0}" + ex.ToString();
            }

            return this.RedirectToAction("Login");
        }

        [Authorize]
        [Route("uitloggen")]
        public ActionResult LogOut()
        {
            HttpContext.Session["year"] = null;
            HttpContext.Session["week"] = null;
            HttpContext.Session["HourModel"] = null;
            HttpContext.Session["UserCode"] = null;
            FormsAuthentication.SignOut();

            Session.Clear();

            return this.RedirectToAction("Login");
        }

        [Authorize]
        [Route("uren/{year}/{weekNumber}")]
        [SessionAvailable]
        public ActionResult Index(int year, int weekNumber, string weekDay = "", string results = "")
        {

            HoursPerWeekModel model = new HoursPerWeekModel();
            ErpOutputOfDailyTimeSlotPE timeSlot = null;

            var holdYear = HttpContext.Session["year"];
            var holdWeek = HttpContext.Session["week"];

            int indirectHours = 0;
            int directHours = 0;

            int userId = 0;

            try
            {
                userId = (int)HttpContext.Session["UserCode"];
            }
            catch (Exception e){
                return this.RedirectToAction("Login");
            }

           //DateTime myDate = this.SundayAsFirstDayOfWeek(year, weekNumber).AddDays(1);
           //timeSlot = this.serviceClient.RetrieveDailyTimeSlot(this.employeeId, userId, firstDate);

            try
            {
                if ((holdYear == null || ((int)holdYear) != year) || ((holdWeek == null) || ((int)holdWeek != weekNumber)))
                {
                    model.EmployeeCode = User.Identity.Name.Split('|')[0];
                    model.LoggedHours = this.serviceClient.ListWeeklyHoursPerEmployee(this.employeeId, year, weekNumber);

                    model.LoggedHours = (from hours in model.LoggedHours
                                         orderby hours.StartTime ascending
                                        select hours).ToArray();

                    HttpContext.Session["year"] = year;
                    HttpContext.Session["week"] = weekNumber;
                    HttpContext.Session["HourModel"] = model;
                }
                else
                {
                    model = (HoursPerWeekModel)HttpContext.Session["HourModel"];
                    model.LoggedHours = this.serviceClient.ListWeeklyHoursPerEmployee(this.employeeId, year, weekNumber);
                    model.LoggedHours = (from hours in model.LoggedHours
                                         orderby hours.StartTime ascending
                                        select hours).ToArray();
                    HttpContext.Session["HourModel"] = model;
                }

                if (model.LoggedHours != null && model.LoggedHours.Count() > 0)
                {
                    int dayNr = -1;
                    if (!string.IsNullOrEmpty(weekDay))
                    {
                        dayNr = this.GetDayNumber(weekDay);
                    }

                    var recentHours = (from hours in model.LoggedHours
                                       where weekDay == string.Empty || (int)hours.StartTime.DayOfWeek == dayNr
                                       orderby hours.StartTime descending, hours.SequenceNr descending
                                       select hours).FirstOrDefault();

                    if (recentHours != null)
                    {
                        timeSlot = this.serviceClient.RetrieveDailyTimeSlot(this.employeeId, userId, recentHours.StartTime);
                        model.NewStartTime = recentHours.EndTime;
                        model.NewStopTime = timeSlot.Entities[0].StopTime;
                        model.Duration = "00:00";
                    }
                }
                else
                {
                    DateTime firstDate = this.SundayAsFirstDayOfWeek(year, weekNumber).AddDays(1);
                    timeSlot = this.serviceClient.RetrieveDailyTimeSlot(this.employeeId, userId, firstDate);
                    model.NewStartTime = timeSlot.Entities[0].StartTime;
                    model.NewStopTime = timeSlot.Entities[0].StopTime;
                    model.Duration = "00:00";
                }
            }

            catch (Exception exception)
            {
                ModelState.AddModelError(string.Empty, exception.Message);
            }
            finally
            {
                ViewBag.WeekDays = this.GetWeekDays();
                ViewBag.DateTime = this.SundayAsFirstDayOfWeek(year, weekNumber).ToString("dd-MM-yyyy");
            }

            string[] weekDatesStart = new string[7];
            string[] weekDatesStop = new string[7];

            for (int i = 0; i < 7; i++)
            {
                DateTime getDate = this.SundayAsFirstDayOfWeek(year, weekNumber).AddDays(i);
                
                timeSlot = this.serviceClient.RetrieveDailyTimeSlot(this.employeeId, userId, getDate);
                model.NewStartTime = timeSlot.Entities[0].StartTime;

                weekDatesStart[i] = timeSlot.Entities[0].StartTime.ToString("HH:mm");
                weekDatesStop[i] = timeSlot.Entities[0].StopTime.ToString("HH:mm");
            }

            ViewBag.weekDatesStart = weekDatesStart;
            ViewBag.weekDatesStop = weekDatesStop;

            UserDefaults userDefaults = (UserDefaults)Session["UserDefaults"];
            ViewBag.methode = userDefaults.method;
            ViewBag.invoerMethode = userDefaults.inputMethod;
            ViewBag.zoom = userDefaults.zoomMethod;

            string CurrentDay = DateTime.Today.DayOfWeek.ToString();

            ViewBag.CurrentDay = GetShortDay(CurrentDay);

            //ViewBag.personal = userDefaults.personalList;

            var opts = new Dictionary<string, DateTime>();

            foreach (WeeklyHoursPerEmployeePE rows in model.LoggedHours)
            {
                string stringKeyStart = rows.Task +"_"+ rows.ProjectCode + "_" + rows.Activity + "_" + rows.ProductionOrder + "_" + rows.Operation;
                string key = Convert.ToString(stringKeyStart + "_" + rows.StartTime.Date.DayOfWeek.GetDayOfWeek());
                if (!opts.ContainsKey(key))
                {
                    opts.Add(key, Convert.ToDateTime(rows.Duration));
                }
                else
                {
                    opts[key] = Convert.ToDateTime(rows.Duration).Add(opts[key].TimeOfDay);
                }
                switch(rows.HourType)
                {
                    case 1:
                        directHours += rows.NormalHours;
                        break;
                    case 2:
                        directHours += rows.NormalHours;
                        break;
                    case 4:
                        indirectHours += rows.NormalHours;
                        break;
                    default:
                        indirectHours += rows.NormalHours;
                        break;
                }
            }
            ViewBag.MachineHours = ConvertTime(indirectHours);
            ViewBag.NormalHours = ConvertTime(directHours);

            this.currentModel = model;

            ViewBag.results = results;

            ViewBag.weekList = opts;

            ViewBag.globalMessage = TempData["sendMessage"];
            TempData["sendMessage"] = "";
            return this.View(model);
        }

        public static string ConvertTime(int secs)
        {
            TimeSpan ts = TimeSpan.FromSeconds(secs);

            string hours = Convert.ToInt32(Math.Floor(ts.TotalHours)).ToString().PadLeft(2, '0'); ;
            string minutes = Convert.ToInt32(ts.Minutes).ToString().PadLeft(2, '0'); ;
            string displayTime = hours + ":" + minutes;
            return displayTime;
        }

        [Authorize]
        [Route("set-date/{currentWeek}")]
        public ActionResult SetDate(string currentWeek)
        {
            DateTime startDate = Convert.ToDateTime(currentWeek.Substring(0, 10));
            int weekNumber = this.GetIso8601WeekOfYear(startDate);
            int currentYear = startDate.Year;

            if (weekNumber == 1 && startDate.Month == 12)
            {
                currentYear++;
            }

            return this.RedirectToAction("Index", new { year = currentYear, weekNumber = weekNumber });
        }

        [HttpPost]
        [Authorize]
        [Route("week-kopieren")]
        public ActionResult CopyWeek (string weekDate)
        { 
            DateTime startDate = Convert.ToDateTime(weekDate.Substring(0, 10));

            DateTime testDate = DateTime.ParseExact(weekDate.Substring(0, 10), "dd-MM-yyyy", CultureInfo.CurrentCulture);

            HoursPerWeekModel model = (HoursPerWeekModel)HttpContext.Session["HourModel"];
            int weekNumber = this.GetIso8601WeekOfYear(Convert.ToDateTime(startDate));
            foreach (WeeklyHoursPerEmployeePE rows in model.LoggedHours)
            {
                string format = "dd-mm-yyyy hh:mm:ss";
                var culture = new System.Globalization.CultureInfo("nl-NL");
                var day = culture.DateTimeFormat.GetDayName(rows.StartTime.DayOfWeek);
                string dayOfWeek = Convert.ToString(day).Substring(0, 2).ToUpper();
                string startTime = Convert.ToDateTime(rows.StartTime).ToString("HH:mm");
                string endTime = Convert.ToDateTime(rows.EndTime).ToString("HH:mm");
                string productionOrder = Convert.ToString(rows.ProductionOrder);
                string operation = Convert.ToString(rows.Operation);
                string task = Convert.ToString(rows.Task);  
                string seqNr = Convert.ToString(rows.SequenceNr);
                InsertHours(startDate.ToString("dd-MM-yyyy"), dayOfWeek, startTime, rows.Duration, endTime, rows.HourType, rows.ProjectCode, rows.Activity, productionOrder, operation, task, seqNr);
            }
            return this.RedirectToAction("Index", new { year = HttpContext.Session["year"], weekNumber = weekNumber});
        }


        [HttpPost]
        [Authorize]
        [Route("weken-invoeren")]
        public ActionResult InsertWeeks(string[] week, string currentWeek, int hourType, string project, string activity, string order, string operation, string task)
        {
            DateTime startDate = Convert.ToDateTime(currentWeek.Substring(0, 10));
            int weekNumber = this.GetIso8601WeekOfYear(startDate);
            int currentYear = startDate.Year;
            for (int i = 0; i < week.Length; i++) 
            {
                string day = "";
                switch (i)
                {
                    case 0: day = "MA"; break;
                    case 1: day = "DI"; break;
                    case 2: day = "WO"; break;
                    case 3: day = "DO"; break;
                    case 4: day = "VR"; break;
                    case 5: day = "ZA"; break;
                    case 6: day = "ZO"; break;
                }
                 if (week[i] != null && week[i] != "00:00" && week[i] != "0")
                {
                    InsertHours(currentWeek, day, "00:00", week[i], "00:00", hourType, project, activity, order, operation, task, "0");
                }
            }

            ViewBag.saveSuccess = TempData["serverresult"];
            return this.RedirectToAction("Index", new { year = currentYear, weekNumber = weekNumber });
        }


        [HttpPost]
        [Authorize]
        [Route("weken-updaten")]
        public ActionResult UpdateWeeks(string[] week, string currentWeek, int hourType, string project, string activity, string order, string operation, string task, int seqNr)
        {
            DateTime startDate = Convert.ToDateTime(currentWeek.Substring(0, 10));
            int weekNumber = this.GetIso8601WeekOfYear(startDate);
            int currentYear = startDate.Year;

            HoursPerWeekModel model = new HoursPerWeekModel();

            model = (HoursPerWeekModel)HttpContext.Session["HourModel"];

            foreach (var LoggedHour in model.LoggedHours)
            {
                if (LoggedHour.HourType == hourType)
                {
                    switch (LoggedHour.HourType)
                    {
                        case 1:

                            //Projecten

                            if(LoggedHour.ProjectCode.ToString() == project && LoggedHour.Activity.ToString() == activity)
                            {
                                Debug.WriteLine("Project " + LoggedHour.HourType.ToString() + " " + " " + LoggedHour.Week.ToString() + " " + LoggedHour.ProjectCode + " " + LoggedHour.Activity);

                                DeleteHours(LoggedHour.EmployeeId, LoggedHour.Week.ToString(), LoggedHour.StartTime.Date, LoggedHour.SequenceNr);
                            }

                            break;

                        case 2:

                            // Productieorders

                            if(LoggedHour.ProductionOrder.ToString() == order && LoggedHour.Operation.ToString() == operation) 
                            {    
                                Debug.WriteLine("Productieorders " + LoggedHour.HourType.ToString() + " ");

                                DeleteHours(LoggedHour.EmployeeId, LoggedHour.Week.ToString(), LoggedHour.StartTime.Date, LoggedHour.SequenceNr);
                            }

                            break;

                        case 4:

                            // Indirecte uren

                            if (LoggedHour.Task.ToString() == task) 
                            { 
                                Debug.WriteLine("Delete indirecte uren " + LoggedHour.HourType.ToString() + " " + LoggedHour.Task + " " + LoggedHour.SequenceNr);

                                DeleteHours(LoggedHour.EmployeeId, LoggedHour.Week.ToString(), LoggedHour.StartTime.Date, LoggedHour.SequenceNr);
                            }

                            break;
                    }
                }
            }

            for (int i = 0; i < week.Length; i++)
            {
                string day = "";
                switch (i)
                {
                    case 0: day = "MA"; break;
                    case 1: day = "DI"; break;
                    case 2: day = "WO"; break;
                    case 3: day = "DO"; break;
                    case 4: day = "VR"; break;
                    case 5: day = "ZA"; break;
                    case 6: day = "ZO"; break;
                }

                if (week[i] != null && week[i] != "00:00" && week[i] != "0") {

                    Debug.WriteLine("Week " + i.ToString() + " " + week[i]);

                    InsertHours(currentWeek, day, "00:00", week[i], "00:00", hourType, project, activity, order, operation, task, seqNr.ToString());
                }
            }
            return this.RedirectToAction("Index", new { year = currentYear, weekNumber = weekNumber });
        }

        [HttpPost]
        [Authorize]
        [Route("uren-invoeren")]
        public ActionResult InsertHours(string currentWeek, string day, string startTime, string duration, string endTime, int hourType, string project, string activity, string order, string operation, string task, string seqNr)
        { 
            Debug.WriteLine("Insert uren via InsertHours");

            DateTime startDate = Convert.ToDateTime(currentWeek.Substring(0, 10));

            int weekNumber = this.GetIso8601WeekOfYear(startDate);
            
            int taskNr = 0;
            
            int currentYear = startDate.Year;
            
            ErpOutputOfWeeklyHoursPerEmployeePE server_result;

            TempData["serverresult"] = "false";
            
            ViewBag.saveSuccess = TempData["serverresult"];
            
            try
            {
                this.ValidateHoursToBeInserted(currentWeek, day, startTime, duration, endTime, hourType, project, activity, order, operation, task);

                if (weekNumber == 1 && startDate.Month == 12)
                {
                    currentYear++;
                }

                WeeklyHoursPerEmployeePE newHoursPerEmployee = new WeeklyHoursPerEmployeePE()
                {
                    EmployeeId = int.Parse(User.Identity.Name.Split('|')[0]),
                    StartTime = this.GetDateTime(startDate, day, startTime),
                    EndTime = this.GetDateTime(startDate, day, endTime),
                    Duration = duration,
                    HourType = hourType,
                    Week = Convert.ToByte(weekNumber),
                    Year = currentYear
                };

                switch (hourType)
                {
                    case 1: //// Hour type Project
                        newHoursPerEmployee.ProjectCode = project.ToUpper();
                        newHoursPerEmployee.Activity = activity.ToUpper();
                        break;

                    case 2: //// Hour type Production
                        if (this.ProductionOrderValidated(order.Trim()))
                        {
                            newHoursPerEmployee.ProductionOrder = int.Parse(order);
                        }

                        if (this.ProductionOperationValidated(operation.Trim()))
                        {
                            newHoursPerEmployee.Operation = int.Parse(operation);
                        }

                        break;

                    case 4: //// Hour type Task
                        if (int.TryParse(task, out taskNr))
                        {
                            newHoursPerEmployee.Task = taskNr;
                        }
                        else
                        {
                            throw new Exception("Taak moet een getal zijn");
                        }

                        break;

                    default:
                        break;
                }

                ErpOutputOfWeeklyHoursPerEmployeePE result = this.serviceClient.InsertHoursAccountingPerEmployee(this.employeeId, newHoursPerEmployee);
                HoursPerWeekModel hourModel = (HoursPerWeekModel)HttpContext.Session["HourModel"];
                server_result = result;
                
                if (result.Success)
                {
                    TempData["serverresult"] = "true";
                    WeeklyHoursPerEmployeePE[] hoursArray = hourModel.LoggedHours;
                    Array.Resize(ref hoursArray, hoursArray.Length + 1);
                    newHoursPerEmployee.SequenceNr = result.Entities[0].SequenceNr;
                    newHoursPerEmployee.Duration = result.Entities[0].Duration;
                    newHoursPerEmployee.Status = result.Entities[0].Status;
                    newHoursPerEmployee.Task = result.Entities[0].Task;
                    newHoursPerEmployee.TaskDescription = result.Entities[0].TaskDescription;
                    newHoursPerEmployee.ProjectDescription = result.Entities[0].ProjectDescription;
                    newHoursPerEmployee.OrderDescription = result.Entities[0].OrderDescription;
                    hoursArray[hoursArray.Length - 1] = newHoursPerEmployee;
                    hourModel.LoggedHours = hoursArray;
                    HttpContext.Session["HourModel"] = hourModel;
                }
                else
                {
                    TempData["serverresult"] = "false";
                    ViewBag.WeekDays = this.GetWeekDays();
                    ViewBag.DateTime = this.SundayAsFirstDayOfWeek(startDate.Year, weekNumber).ToString("dd-MM-yyyy");
                    hourModel.CurrentEntry = this.FillHourModel(day, hourType, project, activity, order, operation, task, startDate, day, startTime, endTime);
                    hourModel.NewStartTime = hourModel.CurrentEntry.StartTime;
                    ViewBag.saveSuccess = TempData["serverresult"];
                    foreach (ErpError error in result.Errors)
                    {
                        TempData["servererror"] = Convert.ToString(error.ErrorMess);
                        ModelState.AddModelError(string.Empty, error.ErrorMess);
                    }
                    return this.View("Index", hourModel);
                }
            }
            catch (Exception ex)
            {
                TempData["serverresult"] = "false";
                ModelState.AddModelError(string.Empty, ex.Message);
                TempData["servererror"] = Convert.ToString(ex.Message);
                HoursPerWeekModel hourModel = (HoursPerWeekModel)HttpContext.Session["HourModel"];
                hourModel.CurrentEntry = this.FillHourModel(day, hourType, project, activity, order, operation, task, startDate, day, startTime, endTime);
                ViewBag.results = ex;
                ViewBag.WeekDays = this.GetWeekDays();
                ViewBag.DateTime = this.SundayAsFirstDayOfWeek(currentYear, weekNumber).ToString("dd-MM-yyyy");
                return this.View("Index", hourModel);
            }
            var jsonSend2BaanResult = new JavaScriptSerializer().Serialize(server_result);
            ViewBag.saveSuccess = TempData["serverresult"];
            return this.RedirectToAction("Index", new { year = currentYear, weekNumber = weekNumber, weekDay = day, results = jsonSend2BaanResult });
        }
        
        [HttpPost]
        [Authorize]
        [Route("uren-updaten")]
        public ActionResult UpdateHours(string currentWeek, int seqNr,  string day, string startTime, string duration, string endTime, int hourType, string project, string activity, string order, string operation, string task)
        {
            DateTime startDate = Convert.ToDateTime(currentWeek.Substring(0, 10));

            int weekNumber = this.GetIso8601WeekOfYear(startDate);
            int taskNr = 0;
            int currentYear = startDate.Year;
            string jsonSend2BaanResult;

            try
            {
                if (weekNumber == 1 && startDate.Month == 12)
                {
                    currentYear++;
                }

                WeeklyHoursPerEmployeePE newHoursPerEmployee = new WeeklyHoursPerEmployeePE()
                {
                    EmployeeId = int.Parse(User.Identity.Name.Split('|')[0]),
                    StartTime = this.GetDateTime(startDate, day, startTime),
                    EndTime = this.GetDateTime(startDate, day, endTime),
                    Duration = duration,
                    HourType = hourType,
                    Week = Convert.ToByte(weekNumber),
                    Year = currentYear,
                    SequenceNr = seqNr
                };

                switch (hourType)
                {
                    case 1: //// Hour type Project
                        newHoursPerEmployee.ProjectCode = project.ToUpper();
                        newHoursPerEmployee.Activity = activity.ToUpper();
                        break;

                    case 2: //// Hour type Production
                        if (this.ProductionOrderValidated(order.Trim()))
                        {
                            newHoursPerEmployee.ProductionOrder = int.Parse(order.Trim());
                        }

                        if (this.ProductionOperationValidated(operation.Trim()))
                        {
                            newHoursPerEmployee.Operation = int.Parse(operation);
                        }

                        break;

                    case 4: //// Hour type Task
                        if (int.TryParse(task, out taskNr))
                        {
                            newHoursPerEmployee.Task = taskNr;
                        }
                        else
                        {
                            throw new Exception("Taak moet een getal zijn");
                        }

                        break;

                    default:
                        break;
                }

                ErpOutputOfWeeklyHoursPerEmployeePE result = this.serviceClient.UpdateHoursAccountingPerEmployee(this.employeeId, newHoursPerEmployee);

                HoursPerWeekModel hourModel = (HoursPerWeekModel)HttpContext.Session["HourModel"];
                jsonSend2BaanResult = new JavaScriptSerializer().Serialize(result);
                if (result.Success)
                {
                    TempData["serverresult"] = "true";
                    WeeklyHoursPerEmployeePE[] hoursArray = hourModel.LoggedHours;
                    Array.Resize(ref hoursArray, hoursArray.Length + 1);
                    newHoursPerEmployee.SequenceNr = seqNr;
                    newHoursPerEmployee.Duration = result.Entities[0].Duration;
                    newHoursPerEmployee.Status = result.Entities[0].Status;
                    newHoursPerEmployee.Task = result.Entities[0].Task;
                    newHoursPerEmployee.TaskDescription = result.Entities[0].TaskDescription;
                    newHoursPerEmployee.ProjectDescription = result.Entities[0].ProjectDescription;
                    newHoursPerEmployee.OrderDescription = result.Entities[0].OrderDescription;
                    hoursArray[hoursArray.Length - 1] = newHoursPerEmployee;
                    hourModel.LoggedHours = hoursArray;
                    HttpContext.Session["HourModel"] = hourModel;
                }
                else
                {
                    TempData["serverresult"] = "false";
                    ViewBag.WeekDays = this.GetWeekDays();
                    ViewBag.DateTime = this.SundayAsFirstDayOfWeek(startDate.Year, weekNumber).ToString("dd-MM-yyyy");
                    hourModel.CurrentEntry = this.FillHourModel(day, hourType, project, activity, order, operation, task, startDate, day, startTime, endTime);
                    hourModel.NewStartTime = hourModel.CurrentEntry.StartTime;

                    foreach (ErpError error in result.Errors)
                    {
                        TempData["servererror"] = Convert.ToString(error.ErrorMess);
                        ModelState.AddModelError(string.Empty, error.ErrorMess);
                    }

                    return this.View("Index", hourModel);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                HoursPerWeekModel hourModel = (HoursPerWeekModel)HttpContext.Session["HourModel"];
                hourModel.CurrentEntry = this.FillHourModel(day, hourType, project, activity, order, operation, task, startDate, day, startTime, endTime);
                TempData["servererror"] = Convert.ToString(ex.Message);
                ViewBag.WeekDays = this.GetWeekDays();
                ViewBag.DateTime = this.SundayAsFirstDayOfWeek(currentYear, weekNumber).ToString("dd-MM-yyyy");
                return this.View("Index", hourModel);
            }

            return this.RedirectToAction("Index", new { year = currentYear, weekNumber = weekNumber, weekDay = day, results = jsonSend2BaanResult });
        }


        [Authorize]
        public ActionResult DeleteWeeks(int employeeId, string referenceNum, string currentWeek, DateTime startDate)
        {
            HoursPerWeekModel hours = (HoursPerWeekModel)HttpContext.Session["HourModel"];
            foreach (WeeklyHoursPerEmployeePE rows in hours.LoggedHours)
            {
                string stringKeyStart = rows.Task + "_" + rows.ProjectCode + "_" + rows.Activity + "_" + rows.ProductionOrder + "_" + rows.Operation;
                string key = Convert.ToString(stringKeyStart);
                if(key == referenceNum)
                {
                    DeleteHours(employeeId, currentWeek, rows.StartTime, rows.SequenceNr);
                }
            }
            return this.RedirectToAction("Index", new { year = startDate.Year, weekNumber = currentWeek });
        }


        [Authorize]
        public ActionResult DeleteHours(int employeeId, string currentWeek, DateTime startDate, int seqNr)
        {
            WeeklyHoursPerEmployeePE deleteHoursPerEmployee = new WeeklyHoursPerEmployeePE()
            {
                EmployeeId = int.Parse(User.Identity.Name.Split('|')[0]),
                StartTime = startDate,
                SequenceNr = seqNr
            };

            ErpOutputOfWeeklyHoursPerEmployeePE result = this.serviceClient.DeleteHoursAccountingPerEmployee(this.employeeId, deleteHoursPerEmployee);

            HoursPerWeekModel hourModel = (HoursPerWeekModel)HttpContext.Session["HourModel"];

            if (result.Success)
            {
                List<WeeklyHoursPerEmployeePE> hoursList = hourModel.LoggedHours.ToList();
                WeeklyHoursPerEmployeePE record = (from hours in hoursList
                                                   where hours.EmployeeId == employeeId && hours.StartTime == startDate && hours.SequenceNr == seqNr
                                                   select hours).FirstOrDefault();

                hoursList.Remove(record);
                WeeklyHoursPerEmployeePE[] hoursArray = hoursList.ToArray();
                hourModel.LoggedHours = hoursArray;
                HttpContext.Session["HourModel"] = hourModel;
            }
            else
            {
                int weekNr = 0;
                int.TryParse(currentWeek, out weekNr);
                ViewBag.WeekDays = this.GetWeekDays();
                ViewBag.DateTime = this.SundayAsFirstDayOfWeek(startDate.Year, weekNr).ToString("dd-MM-yyyy");

                foreach (ErpError error in result.Errors)
                {
                    ModelState.AddModelError(string.Empty, error.ErrorMess);
                }

                return this.View("Index", hourModel);
            }


            return this.RedirectToAction("Index", new { year = startDate.Year, weekNumber = currentWeek });
        }

        //[Authorize]
        //[HttpPost]
        //public ActionResult GetActivitiesByProject(string projectCode)
        //{
        //    List<PublicEntities.ProjectActivityPE> activities = this.activityRepos.RetrieveActivitiesByProject(projectCode);
        //    return this.Json(activities, JsonRequestBehavior.AllowGet);
        //}

        //[Authorize]
        //public PartialViewResult SearchProjectPartialView(string hourType)
        //{
        //    SearchProjectModel searchProject = new SearchProjectModel();
        //    int typeIndex = int.Parse(hourType);
        //    //searchProject.HourType = searchProject.HourTypes()[typeIndex];
        //    searchProject.HourType = int.Parse(hourType);
        //    return PartialView("_SearchProject", searchProject);
        //}

        //[HttpPost]
        //public JsonResult SelectProjectInfo(SearchProjectModel model)
        //{
        //    bool isSuccess = true;
        //    if (ModelState.IsValid)
        //    {
        //        //isSuccess = Save data here return boolean
        //    }
        //    return Json(isSuccess);
        //}

        //[Authorize]
        //public PartialViewResult ListMasterData(int hourType)
        //{
        //    List<ProjectPE> projects = this.projectRepos.RetrieveProjects();
        //    return PartialView("_SearchProject", );
        //}


        private string GetShortDay(string CurrentDay)
        {
            switch (CurrentDay)
            {
                case "Monday":
                    return "MA";
                case "Thuesday":
                    return "DI";
                case "Wednesday":
                    return "WO";
                case "Thursday":
                    return "DO";
                case "Friday":
                    return "VR";
                case "Saturday":
                    return "ZA";
                case "Sunday":
                    return "ZO";
                default:
                    return string.Empty;
            }
        }

        #region Private
        private void ValidateHoursToBeInserted(string currentWeek, string day, string startTime, string duration, string endTime, int hourType, string project, string activity, string order, string operation, string task)
        {
            this.ValidateGenericFields(currentWeek, day, startTime, duration, endTime);

            switch (hourType)
            {
                case 1:
                    this.StringValueFilled("Project", project);
                    this.StringValueFilled("Activiteit", activity);
                    break;
                case 2:
                    this.StringValueFilled("ProductieOrder", order);
                    this.StringValueFilled("Bewerking", operation);
                    break;
                case 4:
                    this.StringValueFilled("Taak", task);
                    break;
                default:
                    break;
            }
        }

        private void ValidateGenericFields(string currentWeek, string day, string startTime, string duration, string endTime)
        {
            this.StringValueFilled("Huidige week", currentWeek);
            this.StringValueFilled("Weekdag", day);
            this.StringValueFilled("Starttijd", startTime);
            this.StringValueFilled("Duur", duration);
            this.StringValueFilled("Stoptijd", endTime);
        }

        private void StringValueFilled(string name, string value)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                throw new Exception(string.Format("De waarde van het veld '{0}' moet gevuld zijn", name));
            }
        }

        private int GetIso8601WeekOfYear(DateTime time)
        {
            DayOfWeek day = CultureInfo.InvariantCulture.Calendar.GetDayOfWeek(time);

            if (day >= DayOfWeek.Sunday && day <= DayOfWeek.Wednesday)
            {
                time = time.AddDays(3);
            }

            return CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(time, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Sunday);
        }

        private DateTime SundayAsFirstDayOfWeek(int year, int weekOfYear)
        {
            DateTime jan1 = new DateTime(year, 1, 1);
            int daysOffset = DayOfWeek.Thursday - jan1.DayOfWeek;

            DateTime firstThursday = jan1.AddDays(daysOffset);
            var cal = CultureInfo.CurrentCulture.Calendar;
            int firstWeek = cal.GetWeekOfYear(firstThursday, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Sunday);

            var weekNum = weekOfYear;

            if (firstWeek <= 1)
            {
                weekNum -= 1;
            }

            var result = firstThursday.AddDays(weekNum * 7);

            return result.AddDays(-4);
        }

        private DateTime FirstDateOfWeekISO8601(int year, int weekOfYear)
        {
            DateTime jan1 = new DateTime(year, 1, 1);
            int daysOffset = DayOfWeek.Thursday - jan1.DayOfWeek;

            DateTime firstThursday = jan1.AddDays(daysOffset);
            var cal = CultureInfo.CurrentCulture.Calendar;
            int firstWeek = cal.GetWeekOfYear(firstThursday, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);

            var weekNum = weekOfYear;

            if (firstWeek <= 1)
            {
                weekNum -= 1;
            }

            var result = firstThursday.AddDays(weekNum * 7);

            return result.AddDays(-3);
        }

        private DateTime GetDateTime(DateTime startDate, string day, string time)
        {
            DateTime date = startDate.AddDays(this.GetDayNumber(day));

            date = date.AddHours(Convert.ToDouble(time.Substring(0, 2)));
            date = date.AddMinutes(Convert.ToDouble(time.Substring(3, 2)));

            return date;
        }

        private int GetDayNumber(string day)
        {
            switch (day)
            {
                case "ZO":
                    return 0;
                case "MA":
                    return 1;
                case "DI":
                    return 2;
                case "WO":
                    return 3;
                case "DO":
                    return 4;
                case "VR":
                    return 5;
                case "ZA":
                    return 6;
                default:
                    throw new ArgumentException("Invalid day");
            }
        }

        private Dictionary<string, string> GetWeekDays()
        {
            Dictionary<string, string> weekDays = new Dictionary<string, string>();
            weekDays.Add("ZO", "Zondag");
            weekDays.Add("MA", "Maandag");
            weekDays.Add("DI", "Dinsdag");
            weekDays.Add("WO", "Woensdag");
            weekDays.Add("DO", "Donderdag");
            weekDays.Add("VR", "Vrijdag");
            weekDays.Add("ZA", "Zaterdag");

            return weekDays;
        }

        private HourModel FillHourModel(string weekDay, int hourType, string project, string activity, string order, string operation, string task, DateTime startDate, string day, string startTime, string endTime)
        {
            int prodOrder = 0;
            int prodOrderOperation = 0;
            int taskNr = 0;

            HourModel hourModel = new HourModel()
            {
                WeekDay = weekDay,
                StartTime = this.GetDateTime(startDate, day, startTime),
                StopTime = this.GetDateTime(startDate, day, endTime),
                HourType = hourType
            };

            switch (hourModel.HourType)
            {
                case 1: //// Hour type Project
                    hourModel.ProjectCode = project;
                    hourModel.Activity = activity;
                    break;
                case 2: //// Hour type Production
                    if (int.TryParse(order, out prodOrder))
                    {
                        hourModel.ProductionOrder = prodOrder;
                    }

                    if (int.TryParse(operation, out prodOrderOperation))
                    {
                        hourModel.Operation = prodOrderOperation;
                    }

                    break;
                case 4: //// Hour type Task
                    if (int.TryParse(task, out taskNr))
                    {
                        hourModel.Task = taskNr;
                    }

                    break;
                default:
                    break;
            }

            return hourModel;
        }

        private bool ProductionOrderValidated(string order)
        {
            int prodOrder = 0;
            if (!int.TryParse(order, out prodOrder))
            {
                throw new Exception("Het ProductieOrderNummer moet een getal van max. 6 cijfers zijn.");
            }

            if (order.Length > 6)
            {
                throw new Exception("Het ProductieOrderNummer mag niet langer dan 6 cijfers zijn." + "[" + order + "]");
            }

            return true;
        }

        private bool ProductionOperationValidated(string operation)
        {
            int prodOrderOperation = 0;

            if (!int.TryParse(operation, out prodOrderOperation))
            {
                throw new Exception("De operatie moet een getal van max. 3 cijfers zijn");
            }

            if (operation.Length > 3)
            {
                throw new Exception("De operatie mag niet langer dan 3 cijfers zijn" + "[" + operation + "]");
            }

            return true;
        }
        #endregion
    }
}
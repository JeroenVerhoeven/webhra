﻿namespace NedTrainHourRegistration.Controllers
{
    using System.Web.Mvc;

    public class ErrorController : Controller
	{
		[Route("error")]
		public ActionResult Index()
		{
			return this.View();
		}

		[Route("not-found")]
		public ActionResult NotFound()
		{
			return this.View();
		}
	}
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using HourRegistration.BusinessLogic.Impl;
using HourRegistration.BusinessLogic.Int;
using HourRegistration.DataAccess.Xml;
using HourRegistration.PublicEntities;
using NedTrainHourRegistration.ViewModels;

namespace NedTrainHourRegistration.Controllers
{  
    public class SearchMasterDataController : Controller
    {
        private IProjectRepository projectRepos;
        private IProjectActivityRepository activityRepos;
        private IIndirectTaskRepository taskRepos;
        
        public SearchMasterDataController()
        {
            this.projectRepos = new ProjectRepository(new ProjectDataAccessor(new XmlAppSettings()));
            this.activityRepos = new ProjectActivityRepository(new ProjectActivityDataAccessor(new XmlAppSettings()));
            this.taskRepos = new IndirectTaskRepository(new IndirectTaskDataAccessor(new XmlAppSettings()));
        }

        [Authorize]
        [HttpPost]
        public ActionResult GetActivitiesByProject(string projectCode)
        {
            List<ProjectActivityPE> activities = this.activityRepos.RetrieveActivitiesByProject(projectCode);
            return this.Json(activities, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public PartialViewResult SearchMasterDataPartialView(string hourType)
        {
            SearchMasterDataModel searchMasterData = new SearchMasterDataModel();
            int typeIndex = int.Parse(hourType);
            searchMasterData.HourType = int.Parse(hourType);
            return PartialView("_SearchMasterData", searchMasterData);
        }

        [Authorize]
        public PartialViewResult SearchActivitiesByProjectPartialView(SearchMasterDataModel model)
        {
            if (!string.IsNullOrEmpty(model.ProjectCode))
            {
                model.ActivitiesByProject = this.activityRepos.RetrieveActivitiesByProject(model.ProjectCode);
            }

            return PartialView("_SearchMasterData", model);
        }

        [HttpPost]
        public JsonResult SelectProjectInfo(SearchMasterDataModel model)
        {
            model.ActivitiesByProject = null;
            if (!string.IsNullOrEmpty(model.ProjectCode))
            {
                model.ActivitiesByProject = this.activityRepos.RetrieveActivitiesByProject(model.ProjectCode);
            }

            return Json(model);
        }

        [Authorize]
        public PartialViewResult ListProjectData()
        {
            List<ProjectPE> projects = this.projectRepos.RetrieveProjects();

            SearchProjectModel searchProjects = new SearchProjectModel()
            {
                Projects = projects,
                FilteredProjects = projects,
                SearchKey = string.Empty
            };

            return PartialView("_ListProjectData", searchProjects);
        }

        [Authorize]
        public PartialViewResult FilterProjects(SearchProjectModel model)
        {
            List<ProjectPE> projects = this.projectRepos.RetrieveProjects();
            model.Projects = projects;
            model.FilteredProjects = projects;

            if (!string.IsNullOrEmpty(model.SearchKey))
            {
                model.FilteredProjects = (from project in projects
                                          where project.SearchString.IndexOf(model.SearchKey, StringComparison.OrdinalIgnoreCase) >= 0
                                          select project).ToList();
            }

            return PartialView("_ListProjectData", model);
        }

        [Authorize]
        public PartialViewResult ListTaskData()
        {
            List<IndirectTaskPE> indirectTasks = this.taskRepos.RetrieveIndirectTasks();

            SearchIndirectTaskModel searchTasks = new SearchIndirectTaskModel()
            {
                IndirectTasks = indirectTasks,
                FilteredTasks = indirectTasks,
                SearchKey = string.Empty
            };

            return PartialView("_ListIndirectTasks", searchTasks);
        }

        [Authorize]
        public PartialViewResult FilterIndirectTasks(SearchIndirectTaskModel model)
        {
            List<IndirectTaskPE> indirectTasks = this.taskRepos.RetrieveIndirectTasks();
            model.IndirectTasks = indirectTasks;
            model.FilteredTasks = indirectTasks;

            if (!string.IsNullOrEmpty(model.SearchKey))
            {
                model.FilteredTasks = (from singleTask in indirectTasks
                                 where singleTask.SearchString.IndexOf(model.SearchKey, StringComparison.OrdinalIgnoreCase) >= 0
                                 select singleTask).ToList();
            }

            return PartialView("_ListIndirectTasks", model);
        }
    }
}
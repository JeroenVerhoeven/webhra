﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace NedTrainHourRegistration.Controllers
{
    [AttributeUsage(AttributeTargets.Method)]
    public class SessionAvailableAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var context = filterContext.HttpContext;
            if (context.Session != null)
            {
                if (context.Session["UserCode"] == null)
                {
                    FormsAuthentication.SignOut();
                    string redirectTo = "~/";
                    if (!string.IsNullOrEmpty(context.Request.RawUrl))
                    {
                        redirectTo = string.Format("~/?ReturnUrl={0}", HttpUtility.UrlEncode(context.Request.RawUrl));
                    }
                    filterContext.HttpContext.Response.Redirect(redirectTo, true);
                }
            }

            base.OnActionExecuting(filterContext);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace NedTrainHourRegistration.Controllers
{
    public class TestMailController : Controller
    {
        // GET: TestMail
        public ActionResult Index()
        {
            return View();
        }
        
        [HttpPost]
        public ActionResult sendMail(FormCollection formFields)
        {
            SmtpClient client = new System.Net.Mail.SmtpClient();

            string to = formFields["mailTo"];
            
            string from = formFields["mailFrom"];

            string server = formFields["server"];

            string poort = formFields["poort"];

            string ssl = formFields["ssl"];

            MailMessage message = new MailMessage(from, to);

            message.Subject = "Test SMTP client.";

            message.Body = @"Test mail met behulp van SMTP Server";

            if(formFields["useCredentials"] == "true")
            {
                client = new System.Net.Mail.SmtpClient
                {
                    Host = server,
                    Port = Convert.ToInt32(poort),
                    EnableSsl = Convert.ToBoolean(ssl),
                    DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = true,
                    Credentials = new System.Net.NetworkCredential("jeroen@treshold.nl", "WelkomTH2014")

                };

            }
            else
            {
                client = new System.Net.Mail.SmtpClient
                {
                    Host = server,
                    Port = Convert.ToInt32(poort),
                    EnableSsl = Convert.ToBoolean(ssl),
                    DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                };
            }

            ViewBag.results = "Mail succesvol verzonden";

            try
            {
                client.Send(message);
            }
            catch (Exception ex)
            {
               ViewBag.results = "Exception caught in CreateTestMessage2(): {0}" + ex.ToString();
            }

            return View();

        }
    }
}
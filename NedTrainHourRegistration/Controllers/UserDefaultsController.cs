﻿using NedTrainHourRegistration.Models;
using NedTrainHourRegistration.NedTrainServiceReference;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace NedTrainHourRegistration.Controllers
{
    public class UserDefaultsController : Controller
    {
        private readonly ERPService_NedTrainSoapClient serviceClient;

        public UserDefaultsController()
        {
            this.serviceClient = new ERPService_NedTrainSoapClient();
        }

        [HttpGet]
        public ActionResult Index()
        {     
            if (User.Identity.IsAuthenticated)
            {
                UserDefaults d = (UserDefaults)Session["UserDefaults"];

                var UserDefaultsFromBaan = new JavaScriptSerializer().Serialize(Session["UserDefaultsFromBaan"]);
                ViewBag.UserDefaultsFromBaan = UserDefaultsFromBaan;

                d.personalList = new List<personalTask> { };

                string ZoekMethod = d.method.ToString();
                methods method = (methods)Enum.Parse(typeof(methods), ZoekMethod);
                d.method = method;

                string ZoekInputMethod = d.inputMethod.ToString();
                inputMethods InputMethod = (inputMethods)Enum.Parse(typeof(inputMethods), ZoekInputMethod);
                d.inputMethod = InputMethod;

                string ZoekZoomMethod = d.zoomMethod.ToString();
                zoomMethods zoomMethod = (zoomMethods)Enum.Parse(typeof(zoomMethods), ZoekZoomMethod);
                d.zoomMethod = zoomMethod;
                ViewBag.CompNr = ConfigurationManager.AppSettings["CompNr"] as string;
                return View(d);
            }

            return Redirect("/");
        }

        [HttpPost]
        public ActionResult saveDetails(FormCollection collection, UserDefaults m)
        {
            UserDefaultsPE defaultsPE = new UserDefaultsPE();
            
            int inputMethod = (int)Enum.Parse(typeof(inputMethods), m.inputMethod.ToString());
            defaultsPE.InputMethod = inputMethod;

            int method = (int)Enum.Parse(typeof(methods), m.method.ToString());
            defaultsPE.Method = method;

            int zoomMethod = (int)Enum.Parse(typeof(zoomMethods), m.zoomMethod.ToString());
            defaultsPE.ZoomMethod = zoomMethod;

            UserDefaults d = (UserDefaults)Session["UserDefaults"];

            var UserDefaultsFromBaan = new JavaScriptSerializer().Serialize(Session["UserDefaultsFromBaan"]);
            ViewBag.UserDefaultsFromBaan = UserDefaultsFromBaan;

            defaultsPE.LoginCode = Convert.ToString(Session["UserLoginCode"]);

            defaultsPE.Password = Convert.ToString(Session["UserPassword"]);

            defaultsPE.EmployeeId = Convert.ToInt32(Session["UserCode"]);

            methods method_method = (methods)Enum.Parse(typeof(methods), m.method.ToString());
            d.method = method_method;

            inputMethods method_InputMethod = (inputMethods)Enum.Parse(typeof(inputMethods), m.inputMethod.ToString());
            d.inputMethod = method_InputMethod;

            zoomMethods method_zoomMethod = (zoomMethods)Enum.Parse(typeof(zoomMethods), m.zoomMethod.ToString());
            d.zoomMethod = method_zoomMethod;
          
            var send2Baan = this.serviceClient.UpdateUserDefaults(d.employeeId, defaultsPE);

            var jsonSsend2Baan = new JavaScriptSerializer().Serialize(defaultsPE);
            ViewBag.jsonSend2Baan = jsonSsend2Baan;

            var jsonSend2BaanResult = new JavaScriptSerializer().Serialize(send2Baan);

            ViewBag.jsonSend2BaanResult = jsonSend2BaanResult;

            ViewBag.message = "Uw wijzigingen zijn succesvol opgeslagen";

            return View("Index", d);
        }
    }
}
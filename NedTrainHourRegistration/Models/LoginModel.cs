﻿namespace NedTrainHourRegistration.Models
{
    using System.ComponentModel.DataAnnotations;

    public class LoginModel
	{
		[Required]
		[Display(Name = "Login code")]
		public string LoginCode { get; set; }

		[Required]
		[Display(Name = "Wachtwoord")]
		public string Password { get; set; }

		[Required]
		[Display(Name = "Bedrijfscode")]
		public int CompNr { get; set; }
	}
}
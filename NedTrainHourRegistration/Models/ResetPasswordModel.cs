﻿namespace NedTrainHourRegistration.Models
{
    using System.ComponentModel.DataAnnotations;

    public class ResetPasswordModel
    {
        [Required]
        [Display(Name = "Login code")]
        public string LoginCode { get; set; }

        [Required]
        [Display(Name = "Bedrijfscode")]
        public int CompanyNr { get; set; }

        [Required]
        [Display(Name = "E-mailadres")]
        public string emailAddress { get; set; }
    }
}
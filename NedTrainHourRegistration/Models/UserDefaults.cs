﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.PeerResolvers;
using System.Web;

namespace NedTrainHourRegistration.Models
{
    public class UserDefaults
    {
        public inputMethods inputMethod { get; set; }
        public methods method { get; set; }
        public zoomMethods zoomMethod { get; set; }
        public string employeeId { get; set; }
        public List<personalTask> personalList { get; set; }
    }


    public enum inputMethods
    {
        Daginvoer = 1,
        Weekinvoer = 2
    }

    public enum zoomMethods
    {
        Baan = 1,
        Gebruiker = 2
    }

    public enum methods
    {
        Starttijd = 1,
        Duur = 2
    }

}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NedTrainHourRegistration.Models
{
    public class personalTask
    {
        public string hourType { get; set; }
        public string Task { get; set; }
        public string ProjectCode { get; set; }
        public string Activity { get; set; }
        public string ProductionOrder { get; set; }
        public string Operation { get; set; }
        public string LabelProjectCode { get; set; }
        public string LabelActivity { get; set; }
        public string LabelTask { get; set; }
    }
}
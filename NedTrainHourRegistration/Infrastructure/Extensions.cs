﻿namespace NedTrainHourRegistration.Infrastructure
{
    using System;

    public static class Extensions
	{
		public static string GetDayOfWeek(this DayOfWeek dayOfWeek)
		{
			switch (dayOfWeek)
			{
				case DayOfWeek.Sunday:
					return "Zondag";
				case DayOfWeek.Monday:
					return "Maandag";
				case DayOfWeek.Tuesday:
					return "Dinsdag";
				case DayOfWeek.Wednesday:
					return "Woensdag";
				case DayOfWeek.Thursday:
					return "Donderdag";
				case DayOfWeek.Friday:
					return "Vrijdag";
				case DayOfWeek.Saturday:
					return "Zaterdag";
				default:
					return string.Empty;
			}
		}

		public static string GetTypeName(this int type)
		{
			switch (type)
			{
				case 1:
					return "1-Project";
				case 2:
					return "2-Productie";
				case 4:
					return "4-Indirect";
				default:
					return string.Empty;
			}
		}

        public static string GetStatusDescription(this int status)
        {
            string statusDescription = string.Empty;

            switch (status)
            {
                case 1:
                    statusDescription = "New";
                    break;
                case 2:
                    statusDescription = "Processed";
                    break;
                case 3:
                    statusDescription = "Error";
                    break;
                case 4:
                    statusDescription = "Cancelled";
                    break;
                case 5:
                    statusDescription = "Sent";
                    break;
            }

            return statusDescription;
        }
	}
}
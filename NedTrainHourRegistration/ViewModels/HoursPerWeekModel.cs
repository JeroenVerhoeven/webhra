﻿namespace NedTrainHourRegistration.ViewModels
{
    using System;
    using System.Collections.Generic;
    using NedTrainHourRegistration.NedTrainServiceReference;
    using System.Linq;
    using HourRegistration.BusinessLogic.Int;
    using HourRegistration.DataAccess.Xml;
    using HourRegistration.BusinessLogic.Impl;
    using PublicEntities = HourRegistration.PublicEntities;
    using HourRegistration.PublicEntities;

    public class HoursPerWeekModel
    {
        private ErpMasterDataProvider dataProvider;
        private string firstProject;

        public HoursPerWeekModel()
        {
            this.dataProvider = new ErpMasterDataProvider();
        }

        public string EmployeeCode { get; set; }

        public WeeklyHoursPerEmployeePE[] LoggedHours { get; set; }

        public HourModel CurrentEntry { get; set; }

        public DateTime NewStartTime { get; set; }

        public DateTime NewStopTime { get; set; }

        public string Duration { get; set; }

        public Dictionary<int, string> HourTypes()
        {
            return StaticEntities.HourTypes();
        }

        public Dictionary<string, string> ErpProjects()
        {
            Dictionary<string, string> projects = this.dataProvider.ErpProjects();

            this.firstProject = string.Empty;
            if (projects != null && projects.Count > 0)
            {
                this.firstProject = ((from project in projects
                                      select project).FirstOrDefault()).Key;
            }

            return projects;
        }

        public Dictionary<string, string> ErpProjectActivities()
        {
            return this.dataProvider.ErpActivitiesByProject(firstProject);
        }

        public Dictionary<int, string> ErpTasks()
        {
            return dataProvider.ErpTasks();
        }

        public string DetermineTotalHours()
        {
            if (this.LoggedHours != null)
            {
                return this.DetermineTotalHours(this.LoggedHours.ToList());
            }

            return string.Empty;
        }

        public string GetProjectDescription(string projectCode)
        {
            return this.dataProvider.ProjectDescription(projectCode);
        }

        public string GetActivityDescription(string projectCode, string activityCode)
        {
            return this.dataProvider.ProjectActivityDescription(projectCode, activityCode);
        }

        protected string DetermineTotalHours(List<WeeklyHoursPerEmployeePE> hoursPerWeek)
        {
            int nrOfHours = 0;
            int nrOfMinutes = 0;

            foreach (WeeklyHoursPerEmployeePE weekHours in hoursPerWeek)
            {
                nrOfHours += this.DetermineNrOfHours(weekHours.Duration);
                nrOfMinutes += this.DetermineNrOfMinutes(weekHours.Duration);
            }

            return this.EvaluateDuration(nrOfHours, nrOfMinutes);
        }

        protected int DetermineNrOfHours(string duration)
        {
            int hours = 0;
            if (!string.IsNullOrEmpty(duration))
            {
                int.TryParse(duration.Substring(0, 2), out hours);
            }

            return hours;
        }

        protected int DetermineNrOfMinutes(string duration)
        {
            int minutes = 0;
            if (!string.IsNullOrEmpty(duration))
            {
                int.TryParse(duration.Substring(3, 2), out minutes);
            }

            return minutes;
        }

        protected string EvaluateDuration(int hours, int minutes)
        {
            while (minutes >= 60)
            {
                hours++;
                minutes -= 60;
            }

            return string.Format("{0:00}:{1:00}", hours, minutes);
        }
    }
}
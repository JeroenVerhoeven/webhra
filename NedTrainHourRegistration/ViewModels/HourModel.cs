﻿namespace NedTrainHourRegistration.ViewModels
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public class HourModel
    {
        public string WeekDay { get; set; }

        public DateTime StartTime { get; set; }

        public DateTime StopTime { get; set; }

        public int SequenceNumber { get; set; }

        public TimeSpan Duration { get; set; }

        [Range(1, 4, ErrorMessage = "Selecteer een Type")]
        public int HourType { get; set; }

        public string ProjectCode { get; set; }

        public string Activity { get; set; }

        public int ProductionOrder { get; set; }

        public int Operation { get; set; }

        public int Task { get; set; }
    }
}
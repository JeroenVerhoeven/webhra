﻿namespace NedTrainHourRegistration.ViewModels
{
    using System.Collections.Generic;
    using System.ComponentModel;
    using HourRegistration.PublicEntities;

    public class SearchMasterDataModel
    {
        public SearchMasterDataModel()
        {
        }

        [DisplayName("Type uren")]
        public int HourType { get; set; }

        [DisplayName("Project")]
        public string ProjectCode { get; set; }

        public string ProjectDescription { get; set; }

        public List<ProjectActivityPE> ActivitiesByProject { get; set; }

        [DisplayName("Activiteit")]
        public string ActivityCode { get; set; }

        [DisplayName("Taak")]
        public string IndirectTask { get; set; }

        public Dictionary<int, string> HourTypes()
        {
            return StaticEntities.HourTypes();
        }
    }
}

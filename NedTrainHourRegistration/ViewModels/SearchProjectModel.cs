﻿using HourRegistration.PublicEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NedTrainHourRegistration.ViewModels
{
    public class SearchProjectModel
    {
        public string SearchKey { get; set; }

        public List<ProjectPE> Projects { get; set; }

        public List<ProjectPE> FilteredProjects { get; set; }
    }
}
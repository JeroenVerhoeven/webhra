﻿using HourRegistration.PublicEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NedTrainHourRegistration.ViewModels
{
    public class SearchIndirectTaskModel
    {
        public string SearchKey { get; set; }

        public List<IndirectTaskPE> IndirectTasks { get; set; }

        public List<IndirectTaskPE> FilteredTasks { get; set; }
    }
}
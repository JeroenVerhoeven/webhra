﻿function OnBlurStartTimeInput(element) {
    ReformatHoursMinutes(element);
    var startTime = element.value;
    var endTimeElement = document.getElementById("endTime");

    if (startTime == "00:00") {
        endTimeElement.value = "00:00";
        document.getElementById("duration").hidden = "";
        document.getElementById("duration").focus();
        document.getElementById("duration").select();
    }
}

function OnBlurStopTimeInput(element) {
    ReformatHoursMinutes(element);
    var stopTime = element.value;
    var hourTypeElement = document.getElementById("hourType");

    if (stopTime != "00:00") {
        document.getElementById("duration").hidden = "hidden";
        hourTypeElement.focus();
    }
}

function ReformatHoursMinutes(element) {
    var time = element.value;

    while (time.length < 4) {
        time = "0" + time;
    }

    if (time.indexOf(":") < 0) {
        time = time.substr(0, time.length - 2) + ":" + time.substr(time.length - 2);
    }

    element.value = time;
}

function ShowButton(element) {
    element.style.visibility = "visible";
}

function HideButton(element) {
    element.style.visibility = "hidden";
}

var alert_text = "";
function CustomValidation() {

    dayinput = document.getElementById("day");
    startTime = document.getElementById("startTime");
    endTime = document.getElementById("endTime");
    duration = document.getElementById("duration");
    hourType = document.getElementById("hourType");
    validationResult = true;
    setters = [false, false, false, false, false];

    if (dayinput && startTime && endTime && duration && hourType) {
    } else {
        return validationResult;
    }

    if (hourType.value == 0) {
        alert_text = "Er moet een project type meegegeven worden voordat de uren opgeslagen kunnen worden";
        validationResult = false;
        invalidInputField(setters);
        setters[4] = true;
        if (document.forms[0].action == "/weken-invoeren") {
            alert(alert_text);
            return validationResult;
        }
    }

    if (dayinput) {
        if (dayinput.value == "") {
            setters[0] = true;
            alert_text = "Er moet een dag geselecteerd worden voordat de uren opgeslagen kunnen worden";
            validationResult = false;
        }
    }

    if (lastType == false) {

        if (!TimeValuesWithinValidRange(duration)) {
            alert_text = "De duur moet een waarde hebben tussen '00:01' en '23:59'";
            setters[3] = true;
            validationResult = false;
        }

        if (!TimeValueGreaterThanZero(duration)) {
            alert_text = "De ingevoerde duur mag niet gelijk zijn aan '00:00'";
            setters[3] = true;
            validationResult = false;
        }
    }
    else {

        if (!TimeValuesWithinValidRange(startTime)) {
            alert_text = "De starttijd moet een waarde hebben tussen '00:01' en '23:58'";
            setters[1] = true;
            validationResult = false;
        }

        if (!TimeValueGreaterThanZero(endTime)) {
            alert_text = 'Ingevoerde uren kunnen niet eindigen om 00:00';
            setters[2] = true;
            validationResult = false;
        }

        if (!StopTimeGreaterThanStartTime(startTime, endTime)) {
            alert_text = "De eindtijd moet later zijn dan de starttijd";
            setters[2] = true;
            validationResult = false;
        }

        if (!TimeValuesWithinValidRange(endTime)) {
            alert_text = "De eindtijd moet een waarde hebben tussen '00:02' en '23:59'";
            setters[2] = true;
            validationResult = false;
        }
    }
    invalidInputField(setters);
    return validationResult;
}

function ValidateDurationOnly(startTimeElement, endTimeElement) {
    durationOnly = false;

    if (startTimeElement.value == "00:00" &&
        endTimeElement.value == "00:00") {
        durationOnly = true;
    }

    return durationOnly;
}

function TimeValueGreaterThanZero(timeElement) {
    if (timeElement) {
        if (timeElement.value == "00:00") {
            return false;
        }
    }
    return true;
}

function StopTimeGreaterThanStartTime(startTimeElement, stopTimeElement) {
    var startHours = parseInt(startTimeElement.value.substr(0, 2));
    var startMinutes = parseInt(startTimeElement.value.substr(3));
    var stopHours = parseInt(stopTimeElement.value.substr(0, 2));
    var stopMinutes = parseInt(stopTimeElement.value.substr(3));

    if ((stopHours < startHours) || (stopHours == startHours && stopMinutes <= startMinutes)) {
        return false;
    }

    return true;
}

function TimeValuesWithinValidRange(timeElement) {
    var timeRangeValid = true;
    if (timeElement) {
        var timeHours = parseInt(timeElement.value.substr(0, 2));
        var timeMinutes = parseInt(timeElement.value.substr(3));

        if (timeHours < 0 || timeMinutes < 0) {
            timeRangeValid = false;
        }

        if (timeHours > 23 || timeMinutes > 59) {
            timeRangeValid = false;
        }
    }
    return timeRangeValid;
}

function HourTypeSelected() {
    var hourTypeSelected = true;
    var hourType = document.getElementById("hourType");
    var selectedType = hourType.options[hourType.selectedIndex].value;
    hourType.setCustomValidity('');

    if (selectedType <= 0) {
        hourType.setCustomValidity('Selecteer een type');
        hourTypeSelected = false;
    }

    return hourTypeSelected;
}
﻿function OnBlurStartTimeInput(element) {
    ReformatHoursMinutes(element);
    var startTime = element.value;
    var endTimeElement = document.getElementById("endTime");

    if (startTime == "00:00") {
        endTimeElement.value = "00:00";
        document.getElementById("duration").hidden = "";
        document.getElementById("duration").focus();
        document.getElementById("duration").select();
    }
}

function OnBlurStopTimeInput(element) {
    ReformatHoursMinutes(element);
    var stopTime = element.value;
    var hourTypeElement = document.getElementById("hourType");

    if (stopTime != "00:00") {
        document.getElementById("duration").hidden = "hidden";
        hourTypeElement.focus();
    }
}

function ReformatHoursMinutes(element) {
    var time = element.value;

    while (time.length < 4) {
        time = "0" + time;
    }

    if (time.indexOf(":") < 0) {
        time = time.substr(0, time.length - 2) + ":" + time.substr(time.length - 2);
    }

    element.value = time;
}

function ShowButton(element) {
    element.style.visibility = "visible";
}

function HideButton(element) {
    element.style.visibility = "hidden";
}

function CustomValidation() {
    validationResult = true;
    var startTime = document.getElementById("startTime");
    var endTime = document.getElementById("endTime");
    var duration = document.getElementById("duration");

    startTime.setCustomValidity("");
    endTime.setCustomValidity("");
    duration.setCustomValidity("");

    if (ValidateDurationOnly(startTime, endTime)) {

        if (!TimeValuesWithinValidRange(duration)) {
            duration.setCustomValidity("De duur moet een waarde hebben tussen '00:01' en '23:59'");
            validationResult = false;
        }

        if (!TimeValueGreaterThanZero(duration)) {
            duration.setCustomValidity("De ingevoerde duur mag niet gelijk zijn aan '00:00'");
            validationResult = false;
        }
    }
    else {

        if (!TimeValuesWithinValidRange(startTime)) {
            startTime.setCustomValidity("De starttijd moet een waarde hebben tussen '00:01' en '23:58'");
            validationResult = false;
        }

        if (!TimeValueGreaterThanZero(endTime)) {
            endTime.setCustomValidity('Ingevoerde uren kunnen niet eindigen om 00:00');
            validationResult = false;
        }

        if (!StopTimeGreaterThanStartTime(startTime, endTime)) {
            endTime.setCustomValidity("De stoptijd moet later zijn dan de starttijd");
            validationResult = false;
        }

        if (!TimeValuesWithinValidRange(endTime)) {
            endTime.setCustomValidity("De stoptijd moet een waarde hebben tussen '00:02' en '23:59'");
            timeRangeValid = false;
        }
    }

    if (!HourTypeSelected()) {
        validationResult = false;
    }

    var msg = document.getElementById('validity').innerHTML;
    return validationResult;
}

function ValidateDurationOnly(startTimeElement, endTimeElement) {
    durationOnly = false;

    if (startTimeElement.value == "00:00" &&
        endTimeElement.value == "00:00") {
        durationOnly = true;
    }

    return durationOnly;
}

function TimeValueGreaterThanZero(timeElement) {
    if (timeElement.value == "00:00") {
        return false;
    }

    return true;
}

function StopTimeGreaterThanStartTime(startTimeElement, stopTimeElement) {
    var startHours = parseInt(startTimeElement.value.substr(0, 2));
    var startMinutes = parseInt(startTimeElement.value.substr(3));
    var stopHours = parseInt(stopTimeElement.value.substr(0, 2));
    var stopMinutes = parseInt(stopTimeElement.value.substr(3));

    if ((stopHours < startHours) || (stopHours == startHours && stopMinutes <= startMinutes)) {
        return false;
    }

    return true;
}

function TimeValuesWithinValidRange(timeElement) {
    var timeRangeValid = true;
    var timeHours = parseInt(timeElement.value.substr(0, 2));
    var timeMinutes = parseInt(timeElement.value.substr(3));

    if (timeHours < 0 || timeMinutes < 0) {
        timeRangeValid = false;
    }

    if (timeHours > 23 || timeMinutes > 59) {
        timeRangeValid = false;
    }

    return timeRangeValid;
}

function HourTypeSelected() {
    var hourTypeSelected = true;
    var hourType = document.getElementById("hourType");
    var selectedType = hourType.options[hourType.selectedIndex].value;
    hourType.setCustomValidity('');

    if (selectedType <= 0) {
        hourType.setCustomValidity('Selecteer een type');
        hourTypeSelected = false;
    }

    return hourTypeSelected;
}
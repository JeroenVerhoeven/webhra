﻿function weekinvoer(projectType) {
    type1 = $(".weektype1");
    type2 = $(".weektype2");
    type3 = $(".weektype4");
    weekdagen = $(".weekdays");
    $(weekdagen).removeClass("col-md-8").removeClass("col-md-9").addClass("col-md-4");
    type1.show();
    type2.show();
    type3.show();
    switch (parseInt(projectType)) {
        case 1:
            $(type2).hide();
            $(type3).hide();
            $(weekdagen).removeClass("col-md-4").addClass("col-md-8");
            break;
        case 2:
            $(type1).hide();
            $(type3).hide();
            $(weekdagen).removeClass("col-md-4").addClass("col-md-9");
            break;
        case 4:
            $(type1).hide();
            $(type2).hide();
            $(weekdagen).removeClass("col-md-4").addClass("col-md-9");
            break;
        default:
            break;
    }
}

function SelectProjectInfo() {
    $.ajax({
        url: SelectProjectInfo,
        type: 'POST',
        data: $("#searchForm").serialize(),
        dataType: "json",
        success: function (data) {
            if (data) {
                //alert(data);
                $(':input', '#searchForm')
                    .not(':button, :submit, :reset, :hidden')
                    .val('')
                    .removeAttr('checked')
                    .removeAttr('selected');

                $("#divSearchMasterData").modal('hide');

                var selectedData = JSON.stringify(data);
                selectedData = JSON.parse(selectedData);
                AssignSelectedMasterData(selectedData)
            }
        }
    });
}

function EnableDisableInputFields(hourType) {
    HideInputFields();
    switch (hourType) {
        case "1":     // Project
            $("#btnSearchMasterProjectData").show();
            $("#ddlProjects").show();
            $("#ddlActivities").show();
            break;
        case "2":     // Production
            $("#txtProductionOrder").show();
            $("#txtOperation").show();
            break;
        case "4":     // Task
            $("#btnSearchMasterTaskData").show();
            $("#ddlTasks").show();
            break;
        default:
            break;
    }
}

function HideInputFields() {
    $("#btnSearchMasterProjectData").hide();
    $("#ddlProjects").hide();
    $("#ddlActivities").hide();
    $("#txtProductionOrder").hide();
    $("#txtOperation").hide();
    $("#btnSearchMasterTaskData").hide();
    $("#ddlTasks").hide();
}

function AssignSelectedMasterData(selectedData) {
    $('#hourType').val(selectedData.HourType);
    $('#hourType').change();

    if (selectedData.HourType == 1)     // Project
    {
        AssignSelectedProjectData(selectedData);
        $('#txtProductionOrder').hide();
        $('#txtOperation').hide();
    }

    if (selectedData.HourType == 2)     // Production
    {
        $('#ddlProjects').hide();
        $('#ddlActivities').hide();
        $('#txtProductionOrder').show();
        $('#txtOperation').show();
    }
}

function returnDate(direction, last = false) {
    if (last) {
        var value = $(".weeklyDatePicker:last").datepicker({ dateFormat: 'dd-mm-yy' }).val();
    } else {
        var value = $(".weeklyDatePicker").datepicker({ dateFormat: 'dd-mm-yy' }).val();
    }
    var firstDate = moment(value, "DD-MM-YYYY").day(0).format("YYYY/MM/DD");
    var today = new Date(firstDate);
    var MyDate = new Date(today.setDate(today.getDate() + direction));
    return MyDateString = ('0' + MyDate.getDate()).slice(-2) + '-'
        + ('0' + (MyDate.getMonth() + 1)).slice(-2) + '-'
        + MyDate.getFullYear();
}

function AssignSelectedProjectData(selectedData) {
    $('#ddlProjects').show();
    $('#ddlProjects').val(selectedData.ProjectCode);
    AssignProjectActivities(selectedData.ActivitiesByProject);
    $('#ddlActivities').show();
    $('#ddlActivities').val(selectedData.ActivityCode);
}

function AssignProjectActivities(activities) {
    $("#ddlActivities").empty();
    $.each(activities, function (index) {
        var actCode = activities[index].ActivityCode;
        var description = activities[index].Description;
        $("#ddlActivities").append("<option value=\"" + actCode + "\">" + actCode + " " + description + "</option>");
    });
}

function CustomCalcWeek(date) {
    var checkDay = moment(date, "DD-MM-YY");
    var checkDate = checkDay.toDate();
    checkDate.setDate(checkDate.getDate() + 1);
    var week = $.datepicker.iso8601Week(checkDate);
    return week;
}

function runSelectPicker() {
    $(".selectpicker").select2();
    $('.selectpicker').each(function () {
        $(this).next("span").attr("style", $(this).attr("style"));

        var selectField2 = $(this).next("span").find(".select2-selection__rendered");
        selectField2.addClass("form-control-select2");
    });
}

function filter() {
    var searchKey = $("#SearchKey").val();
    var $parentDiv = $("#SearchKey").closest('form').closest('div');
    var link = Filter;
    link = link.replace("varSearchKey", searchKey);
    $parentDiv.load(link);
}

//false is duur!
function setInvoerType(default_type = true) {
    $(document).find("#startTime, #endTime, #use_duration").css('display', default_type ?'': 'none');
    $(document).find("#duration, #use_start_end").css('display', !default_type ? '' : 'none');
    lastType = default_type;
}

function searchLink(link) {
    $('#divSearchData').load(link, function (result) {
        $('#divSearchMasterData').modal({
            show: true
        });
    });
}

function getCopyWeek(value) {
    var firstDate = moment(value, "DD-MM-YYYY").day(0).format("DD-MM-YYYY");
    var lastDate = moment(value, "DD-MM-YYYY").day(6).format("DD-MM-YYYY");
    $(".weeklyDatePicker:last").val(firstDate + " - " + lastDate);
}

function getRowData(parent, copied = false) {
    if (!copied) {
        $(".row.tablerow.editable #day").val($(parent).find(".ed-daytime").text().substr(0, 2).toUpperCase());
    } else {
        $(".row.tablerow.editable #day").val("");
    }
    $(".row.tablerow.editable #hourType").val($(parent).find(".ed-type").text().substr(0, 1)).change();
    $(".row.tablerow.editable #ddlTasks").val($(parent).find(".ed-task label").text()).change();
    $(".row.tablerow.editable #ddlProjects").val($(parent).find(".ed-projectc label").text()).change();
    $(".row.tablerow.editable #startTime").val($(parent).find(".ed-starttime").text());
    $(".row.tablerow.editable #endTime").val($(parent).find(".ed-endtime").text());
    $(".row.tablerow.editable #duration").val($(parent).find(".ed-duration").text());
    $(".row.tablerow.editable #txtProductionOrder").val($(parent).find(".ed-porder").text());
    $(".row.tablerow.editable #txtOperation").val($(parent).find(".ed-operation").text());
    $(".row.tablerow.editable #seqNr").val($(parent).find(".ed-seqnr").text());
    if ($("#startTime").val() == "00:00" && $("#endTime").val() == "00:00") {
        setInvoerType(false);
    } else {
        setInvoerType();
    }
    $(".addRow").show();
    setTimeout(function () {
        $(".row.tablerow.editable #ddlActivities").val($(parent).find(".ed-activity label").text());
        runSelectPicker();
    }, 100);
    runSelectPicker();
}

function warnUser() {
    if ($("#hourType").val() && $("#hourType").val() != "0") {
        if (!confirm('Niet opgeslagen wijzigingen verdwijnen wanneer u doorgaat. Weet u zeker dat u verder wilt gaan?')) {
            return true;
        }
    }
    return false;
}

var lastDayWas = "";
function getPrevTime(time = null) {
    if ($(".editable").prev(".tablerow").find(".ed-daytime").length > 0) {
        lastDayWas = $(".editable").prev(".tablerow").find(".ed-daytime").text().substring(0, 2).toUpperCase();
    }
    if (time == null) {
        time = $(".editable").prev(".tablerow").find(".ed-endtime").text()
    }
    if (time == "--:--" || time == "") {
        time = "00:00"
    }
    $("#day").val(lastDayWas);
    $("#startTime").val(time);
    $("#endTime").val("00:00");
}

function invalidInputField(setters) {
    if ($.isArray(setters) && setters.length > 4) {
        $("#day").toggleClass('invalid', (setters[0]));
        $("#startTime").toggleClass('invalid', (setters[1]));
        $("#endTime").toggleClass('invalid', (setters[2]));
        $("#duration").toggleClass('invalid', (setters[3]));
        $("#hourType").toggleClass('invalid', (setters[4]));
    } 
}

function sendHoursOnEnter() {
    if (busy) {
        alert("Wacht tot het vorige proces voltooid is!");
        return;
    }
    if (CustomValidation()) {
        var form = $("form:first");
        var url = form.attr('action');
        if (lastType == false) {
            $("#startTime").val("00:00");
            $("#endTime").val("00:00");  
        } else {
            $("#duration").val("00:00");
        }
        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(),
            beforeSend: function () {
                busy = true;
                if ($("#hourType").val() == 2) {

                } else {
                    $(".row.tablerow.editable").addClass("saving").prepend('<div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div>');
                    if (!update) {
                        endTimeWas = $(".editable #endTime").val();
                        lastDayWas = $(".editable #day").val();
                        $(document).find(".row.tablerow.editable *").attr("id", "");
                        $(document).find(".row.tablerow.editable .saveRowButton").hide()
                        $(document).find(".row.tablerow.editable").after(currentEditableRow);
                        getPrevTime(endTimeWas);
                        HideInputFields();
                        setInvoerType(lastType);
                    }
                    $(".row.tablerow.saving *").attr("id", "");
                    setInvoerType(lastType);
                }
            },
            success: function (data) {
                errors = $(data).find("#success").val() == "False" ? true : false
                if (!errors) {
                    var elements = $(data);
                    if (!update) {
                        var found = $('#hour-registration-table', elements).find(".row.tablerow").eq(-2);
                        $(".row.tablerow.saving").remove();
                        $(".row.tablerow:last").before(found);
                        $("#hourTab").replaceWith($(data).find("#hourTab"));
                    } else {
                        location.reload();
                    }
                } else {
                    $("#showIssue").text($(data).find("#showIssue").text());
                    $("#showIssue").show();
                }
            },
            complete: function () {
                busy = false;
            }
        });
    } else {
        alert(alert_text)
    }
}
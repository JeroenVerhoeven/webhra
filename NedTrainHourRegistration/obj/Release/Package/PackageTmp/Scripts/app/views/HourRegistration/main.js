﻿$(document).ready(function () {

    // Opslaan of Updaten van uren
    $(document).on("keydown", ".editable", function (e) {
            if (e.keyCode == 13) {
                if ($("#formaction").val() == "/weken-invoeren") {
                    $(".row.tablerow.editable").addClass("saving").prepend('<div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div>');
                    $("form:first").submit();
                } else {
                    sendHoursOnEnter();
                }
            }
     });

    // Filteren tussen projecten wanneer er op enter gedrukt wordt
        $(document).on("keydown", "#SearchKey", function (e) {
            if (e.keyCode == 13) {
                e.preventDefault();
                filter();
            }
        });

    //Filter knoppen in .row.table-row.editable
        $(document).on('click', "#btnSearchMasterData", function () {
            searchLink(SearchMasterDataPartialViewvarHourType.replace("varHourType", $("#hourType").val()));
        });

        $(document).on('click', "#btnSearchMasterTaskData", function () {
            searchLink(SearchMasterDataPartialView4)
        });

        $(document).on('click', "#btnSearchMasterProjectData", function () {
            searchLink(SearchMasterDataPartialView1)
        });

        $(document).on('click', "#btnSearchMasterData-2", function () {
            var link = SearchMasterDataPartialViewvarHourType.replace("varHourType", $("#hourType").val());

            $("#divSearchMasterData-2").dialog({
                autoOpen: true,
                title: "Search Master Data",
                resizable: true,
                open: function () {
                    $(this).load(link);
                },
                buttons: {
                    "OK": function () {
                        SelectProjectInfo();
                    },
                    "Annuleren": function () {
                        $(this).dialog('close');
                    }
                }
            });

            return false;
        });
    
    //Persoonlijke voorkeuren ophalen 
    $(document).on("change", "#selectHourType", function () {
        var selected = $("#selectHourType option:selected");
        HideInputFields();
        switch (selected.data("hourtype")) {
            case 1:
                    $(".row.tablerow.editable #ddlProjects").val(selected.data("projectcode"));
                    setTimeout(function () {
                        $(".row.tablerow.editable #ddlActivities").val(selected.data("activity"));
                    }, 100);
                    break;
            case 4:
                    $(".row.tablerow.editable #ddlTasks").val(selected.data("task")).change();
                    break;
            default:
                    HideInputFields();
                    break;
        }
        runSelectPicker();
        });

    // Week selecteren
        $(document).on("click", '#prevWeek', function () {
            MyDateString = returnDate(-7);
            window.location.href = '/set-date/' + MyDateString;
        });

        $(document).on("click", '#nextWeek', function () {
            MyDateString = returnDate(7);
            window.location.href = '/set-date/' + MyDateString;
        });


    // Week selecteren voor kopiëren
        $(document).on("click", '#copyPrevWeek', function () {
            getCopyWeek(returnDate(-7, true));
        });

        $(document).on("click", '#copyNextWeek', function () {
            getCopyWeek(returnDate(7, true));
        });


    // Regel Kopiëren
        $(document).on("click", ".fa-copy", function () {
            if (warnUser()) {
                return
            }
            var parent = $(this).closest('.row.tablerow');
            $(".row.tablerow").show();
            update = false;
            $("form:first").attr("action", "/uren-invoeren");
            $('.row.tablerow.editable').remove();
            $(".row.tablerow:last").after(currentEditableRow);
            $('.row.tablerow.editable').find('.copied').css({ display: "inherit" });
            getRowData(parent, true);
        });

    // Regel Aanpassen
        $(document).on("click", ".fa-edit", function () {
            if (warnUser()) {
                return
            }
            var parent = $(this).closest('.row.tablerow');
            $(".row.tablerow").show();
            update = true;
            $("form:first").attr("action", "/uren-updaten");
            $('.row.tablerow.editable').remove();
            $(parent).before(currentEditableRow);
            $(parent).hide();
            $(".removeRow").hide();
            getRowData(parent);
        });

    // Regel toevoegen
    $(document).on("click", ".addRow", function () {
        if (warnUser()) {
            return
        }
            if ($("#formaction").val() == "/weken-invoeren") {
                if ($(".row.tablerow:last").length > 0) {
                    $(".row.tablerow:last").after(currentEditableRow);
                } else {
                    $("#hour-registration-table .row:first").after(currentEditableRow);
                }
                $(".saveWeek").show();
                HideInputFields();
                $(".addRow").hide();
            } else {
                $(".row.tablerow").show();
                $('.row.tablerow.editable').remove();
                update = false;
                $("form:first").attr("action", $("#formaction").val());
                $(".addRow").hide();
                if ($(".row.tablerow:last").length > 0) {
                    $(".row.tablerow:last").after(currentEditableRow);
                } else {
                    $("#hour-registration-table .row:first").after(currentEditableRow);
                }
                setInvoerType(lastType);
                getPrevTime();
                $(".removeRow").show();
                HideInputFields();
            }
        });

    // Regel verwijderen
        $(document).on("click", ".removeRow", function () {
            if (warnUser()) {
                return
            }
            $(".addRow").show();
            $('.row.tablerow.editable').remove();
            $(".saveWeek").hide();
        });

    // Haal projecten op
        $(document).on("change", "#hourType", function () {
            weekinvoer($(this).val());
            var hourType = $("#hourType").val();
            EnableDisableInputFields(hourType);
            runSelectPicker();
        })

    // Projecten ophalen wanneer dit geselecteerd wordt 
        $(document).on('change', "#ddlProjects", function () {
            var projectCode = $("#ddlProjects Option:Selected").val();
            $.ajax({
                type: 'POST',
                url: GetActivitiesByProject,
                data: { 'projectCode': projectCode },
                dataType: 'json',
                cache: false,
                success: function (result) {
                    var activities = eval('(' + JSON.stringify(result) + ')');
                    $("#ddlActivities").empty();
                    $.each(activities, function (index) {
                        var actCode = activities[index].ActivityCode;
                        var description = activities[index].Description;
                        $("#ddlActivities").append("<option value=\"" + actCode + "\">" + actCode + " " + description + "</option>");
                    });
                },
                error: function (request, status, error) {
                    alert(request.responseText);
                    alert("Call Failed. Please Try Again");
                }
            });
        });

    // Gegevens ophalen van de weekpicker
        $(document).on("change", ".weeklyDatePicker", function (e) {
            var value = $(".weeklyDatePicker").datepicker({ dateFormat: 'dd-mm-yy' }).val();
            var firstDate = moment(value, "DD-MM-YYYY").day(0).format("DD-MM-YYYY");
            var lastDate = moment(value, "DD-MM-YYYY").day(6).format("DD-MM-YYYY");
            $(".weeklyDatePicker").val(firstDate + " - " + lastDate);
            if (firstDate != LayoutDateTime) {
                var currentWeek = $(".weeklyDatePicker").val();
                window.location.href = '/set-date/' + value;
            }
        });

    //Regel controleren 
        $(document).on("change", "#endTime, #startTime", function () {
            dayinput = document.getElementById("day");
            startTime = document.getElementById("startTime");
            endTime = document.getElementById("endTime");
            duration = document.getElementById("duration");
            hourType = document.getElementById("hourType");

            inactiveStart = false;
            inactiveEnd = false;

            if ($("#endTime").val() == "00:00") {
                return;
            }

            alert_text = "";
            if (!StopTimeGreaterThanStartTime(startTime, endTime)) {
                alert_text = "De eindtijd moet later zijn dan de starttijd";
                inactiveEnd = true;
            }

            if (inactiveStart) {
                $("#startTime").addClass("invalid");
            } else {
                $("#startTime").removeClass("invalid");
            }
            
            if (inactiveEnd) {
                $("#endTime").addClass("invalid");
            } else {
                $("#endTime").removeClass("invalid");
            }

            if (alert_text != "") {
                alert(alert_text);
            }
        })

    // Datepicker maar dan in het nederlands
        $.datepicker.regional['nl'] = {
            closeText: 'Sluiten', closeStatus: 'Onveranderd sluiten ',
            prevText: 'Vorige', prevStatus: 'Zie de vorige maand',
            nextText: 'Volgende', nextStatus: 'Zie de volgende maand',
            currentText: 'Huidige', currentStatus: 'Bekijk de huidige maand',
            monthNames: ['Januari', 'Februari', 'Maart', 'April', 'Mei', 'Juni',
                'Juli', 'Augustus', 'September', 'Oktober', 'November', 'December'],
            monthNamesShort: ['Jan', 'Feb', 'Mrt', 'Apr', 'Mei', 'Jun',
                'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dec'],
            monthStatus: 'Bekijk een andere maand', yearStatus: 'Bekijk nog een jaar',
            weekHeader: 'Wk', weekStatus: '',
            dayNames: ['Zondag', 'Maandag', 'Dinsdag', 'Woensdag', 'Donderdag', 'Vrijdag', 'Zaterdag'],
            dayNamesShort: ['Zo', 'Ma', 'Di', 'Wo', 'Do', 'Vr', 'Za'],
            dayNamesMin: ['Zo', 'Ma', 'Di', 'Wo', 'Do', 'Vr', 'Za'],
            dayStatus: 'Gebruik DD als de eerste dag van de week', dateStatus: 'Kies DD, MM d',
            dateFormat: 'dd/mm/yy', firstDay: 1,
            initStatus: 'Kies een datum', isRTL: false
        };
        $.datepicker.setDefaults($.datepicker.regional['nl']);
});
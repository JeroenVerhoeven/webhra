﻿// Zet alle variable of voer alles uit wat uitgevoerd moet worden om de applicatie te laten werken

var firstDate = moment(LayoutDateTime, "DD-MM-YYYY").day(0).format("DD-MM-YYYY");

var lastDate = moment(LayoutDateTime, "DD-MM-YYYY").day(6).format("DD-MM-YYYY");

var currentEditableRow = $(".row.tablerow.editable")[0].outerHTML;

var enable_enter = false;

var lastType = true;

var update = false;

var noEdit = true;

var busy = false;

var dayinput

var dayinput;

var startTime;

var endTime;

var duration;

var hourType;

// Construct

$(window).ready(function () {

    HideInputFields();

    if ($('#invoerMethode').val() == "Duur") {
        OnBlurStartTimeInput(document.getElementById("startTime"));
        setInvoerType(false)
    } else {
        setInvoerType();
    }

    $(".weeklyDatePicker").val(firstDate + " - " + lastDate);

    $('[data-toggle="tooltip"]').tooltip();

    $(".form-header").prependTo("form:first");

    $(".weeklyDatePicker").datepicker({
        dateFormat: 'dd-mm-yy',
        calculateWeek: CustomCalcWeek,
        firstDay: 0,
        showWeek: true
    });

    $('.modal-dialog').draggable();

    //$(".sortpersonal").each(function () {
    //    $(this).appendTo($("optgroup[data-hourtype='"+$(this).data("hourtype")+"']"))
    //})

    getPrevTime();

    $(".editable").remove();

    runSelectPicker();

});

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace NedTrainHourRegistration.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            ScriptBundle initBundle = new ScriptBundle("~/bundles/master/init/js");

            initBundle
                .Include("~/Scripts/jquery-3.2.1.min.js")
                .Include("~/Scripts/jquery-ui.js")
                .Include("~/Scripts/bootstrap.datepicker.min.js")
                .Include("~/Scripts/bootstrap.datetimepicker.min.js")
                .Include("~/Scripts/bootstrap.select.js")
                .Include("~/Scripts/bootstrap.min.js")
                .Include("~/Scripts/select2.min.js")
                .Include("~/Scripts/timepicker.min.js")
                .Include("~/Scripts/HoursRegistration.js");

            bundles.Add(initBundle);

            ScriptBundle customBundle = new ScriptBundle("~/bundles/master/custom/js");

            customBundle
                .Include("~/Scripts/app/functions.js")
                .Include("~/Scripts/app/init.js")
                .Include("~/Scripts/app/main.js")
                .Include("~/Scripts/app/views/HourRegistration/main.js");

            bundles.Add(customBundle);

            StyleBundle styleBundle = new StyleBundle("~/bundles/master/css");

            styleBundle
                .Include("~/Content/css/font-awesome.min.css")
                .Include("~/Content/bootstrap-datepicker.min.css")
                .Include("~/Content/bootstrap-datepicker.standalone.min.css")
                .Include("~/Content/bootstrap-datepicker3.min.css")
                .Include("~/Content/bootstrap-datepicker3.standalone.min.css")
                .Include("~/Content/bootstrap-theme.min.css")
                .Include("~/Content/bootstrap.min.css")
                .Include("~/Content/jquery-ui.css")
                .Include("~/Content/jquery-ui.structure.css")
                .Include("~/Content/jquery-ui.theme.css")
                .Include("~/Content/select2.css")
                .Include("~/Content/timepicker.min.css")
                .Include("~/Content/Site.css");

            bundles.Add(styleBundle);

            BundleTable.EnableOptimizations = true;
        }
    }
}
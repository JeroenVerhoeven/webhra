﻿using HourRegistration.BusinessLogic.Int;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HourRegistration.DataEntities;
using HourRegistration.DataAccess.Int;
using HourRegistration.PublicEntities;
using HourRegistration.Utilities;

namespace HourRegistration.BusinessLogic.Impl
{
    public class ProjectActivityRepository : IProjectActivityRepository
    {
        private IDataAccessor<ProjectActivity> activityAccessor;

        public ProjectActivityRepository(IDataAccessor<ProjectActivity> dataAccessor)
        {
            this.activityAccessor = dataAccessor;
        }

        public List<ProjectActivityPE> RetrieveActivitiesByProject(string projectCode)
        {
            List<ProjectActivity> dataProjectActivities = this.activityAccessor.RetrieveEntityListByCode(projectCode);
            List<ProjectActivityPE> publicProjectActivities = BaseConverter<ProjectActivity, ProjectActivityPE>.ConvertToEntityList(dataProjectActivities);
            return publicProjectActivities;
        }

        public ProjectActivityPE RetrieveSingleProjectActivity(string projectCode, string activityCode)
        {
            ProjectActivity dataProjectActivity = this.activityAccessor.RetrieveSingleEntityByParentCode(projectCode, activityCode);
            ProjectActivityPE publicProjectActivity = BaseConverter<ProjectActivity, ProjectActivityPE>.ConvertToSingleEntity(dataProjectActivity);
            return publicProjectActivity;
        }
    }
}

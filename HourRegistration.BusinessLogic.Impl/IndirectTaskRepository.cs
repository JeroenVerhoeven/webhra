﻿using HourRegistration.BusinessLogic.Int;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HourRegistration.DataEntities;
using HourRegistration.PublicEntities;
using HourRegistration.DataAccess.Int;
using HourRegistration.Utilities;

namespace HourRegistration.BusinessLogic.Impl
{
    public class IndirectTaskRepository : IIndirectTaskRepository
    {
        private IDataAccessor<IndirectTask> tasksAccessor;

        public IndirectTaskRepository(IDataAccessor<IndirectTask> dataAccessor)
        {
            this.tasksAccessor = dataAccessor;
        }

        public List<IndirectTaskPE> RetrieveIndirectTasks()
        {
            List<IndirectTask> dataTasks = this.tasksAccessor.RetrieveListOfEntities();
            List<IndirectTaskPE> publicTasks = BaseConverter<IndirectTask, IndirectTaskPE>.ConvertToEntityList(dataTasks);
            return publicTasks;
        }

        public IndirectTaskPE RetrieveSingleTask(int taskCode)
        {
            IndirectTask dataTask = this.tasksAccessor.RetrieveSingleEntity(taskCode.ToString());
            IndirectTaskPE publicTask = BaseConverter<IndirectTask, IndirectTaskPE>.ConvertToSingleEntity(dataTask);
            return publicTask;
        }
    }
}

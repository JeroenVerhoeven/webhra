﻿using HourRegistration.BusinessLogic.Int;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HourRegistration.DataEntities;
using HourRegistration.DataAccess.Int;
using HourRegistration.PublicEntities;
using HourRegistration.Utilities;

namespace HourRegistration.BusinessLogic.Impl
{
    public class ProjectRepository : IProjectRepository
    {
        private IDataAccessor<Project> projectAccessor;

        public ProjectRepository(IDataAccessor<Project> dataAccessor)
        {
            this.projectAccessor = dataAccessor;
        }

        public List<ProjectPE> RetrieveProjects()
        {
            List<Project> dataProjects = this.projectAccessor.RetrieveListOfEntities();
            List<ProjectPE> publicProjects = BaseConverter<Project, ProjectPE>.ConvertToEntityList(dataProjects);
            return publicProjects;
        }

        public ProjectPE RetrieveSingleProject(string projectCode)
        {
            Project dataProject = this.projectAccessor.RetrieveSingleEntity(projectCode);
            ProjectPE publicProject = BaseConverter<Project, ProjectPE>.ConvertToSingleEntity(dataProject);
            return publicProject;
        }
    }
}

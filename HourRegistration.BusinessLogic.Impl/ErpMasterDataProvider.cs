﻿using HourRegistration.BusinessLogic.Int;
using HourRegistration.DataAccess.Xml;
using HourRegistration.PublicEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HourRegistration.BusinessLogic.Impl
{
    public class ErpMasterDataProvider
    {
        private IProjectRepository projectRepos;
        private IProjectActivityRepository activityRepos;
        private IIndirectTaskRepository taskRepos;

        public ErpMasterDataProvider()
        {
            this.projectRepos = new ProjectRepository(new ProjectDataAccessor(new XmlAppSettings()));
            this.activityRepos = new ProjectActivityRepository(new ProjectActivityDataAccessor(new XmlAppSettings()));
            this.taskRepos = new IndirectTaskRepository(new IndirectTaskDataAccessor(new XmlAppSettings()));
        }

        public Dictionary<string, string> ErpProjects()
        {
            Dictionary<string, string> erpProjects = new Dictionary<string, string>();
            string projectCodeDescr = null;
            List<PublicEntities.ProjectPE> projects = this.projectRepos.RetrieveProjects();

            foreach (PublicEntities.ProjectPE project in projects)
            {
                projectCodeDescr = string.Format("{0} {1}", project.Code, project.Description);
                erpProjects.Add(project.Code, projectCodeDescr);
            }

            return erpProjects;
        }

        public string ProjectDescription(string projectCode)
        {
            ProjectPE project = this.projectRepos.RetrieveSingleProject(projectCode);
            return project != null ? project.Description : null;
        }

        public Dictionary<string, string> ErpActivitiesByProject(string project)
        {
            Dictionary<string, string> erpActivities = new Dictionary<string, string>();
            List<PublicEntities.ProjectActivityPE> projectActivities = this.activityRepos.RetrieveActivitiesByProject(project);
            string activityDescription = null;

            foreach (PublicEntities.ProjectActivityPE activity in projectActivities)
            {
                activityDescription = string.Format("{0} {1}", activity.ActivityCode, activity.Description);
                erpActivities.Add(activity.ActivityCode, activityDescription);
            }

            return erpActivities;
        }

        public string ProjectActivityDescription(string projectCode, string activityCode)
        {
            ProjectActivityPE activity = this.activityRepos.RetrieveSingleProjectActivity(projectCode, activityCode);
            return activity != null ? activity.Description : null;
        }

        public Dictionary<int, string> ErpTasks()
        {
            Dictionary<int, string> erpTasks = new Dictionary<int, string>();
            string taskNrDescr = null;
            List<PublicEntities.IndirectTaskPE> indirectTasks = this.taskRepos.RetrieveIndirectTasks();

            foreach (PublicEntities.IndirectTaskPE task in indirectTasks)
            {
                taskNrDescr = string.Format("{0} {1}", task.Task, task.Description);
                erpTasks.Add(task.Task, taskNrDescr);
            }

            return erpTasks;
        }
    }
}

﻿using HourRegistration.DataAccess.Int;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace HourRegistration.DataAccess.Xml
{
    public class XmlAppSettings : IAppSettings
    {
        private string xmlDataFolder;

        public string XmlDataFolder
        {
            get
            {
                if (string.IsNullOrEmpty(this.xmlDataFolder))
                {
                    string dataFolder = AppDomain.CurrentDomain.BaseDirectory;
                    this.xmlDataFolder = this.DetermineAppSetting(DAConstants.XmlDataFolder, dataFolder);
                }

                return this.xmlDataFolder;
            }
        }

        protected virtual string DetermineAppSetting(string keyName, string defaultValue)
        {
            if (string.IsNullOrEmpty(keyName))
            {
                return defaultValue;
            }

            string configValue = this.DetermineAppSettingValue(keyName);
            return string.IsNullOrEmpty(configValue) ? defaultValue : configValue;
        }

        protected virtual string DetermineAppSettingValue(string keyName)
        {
            return ConfigurationManager.AppSettings[keyName] as string;
        }
    }
}

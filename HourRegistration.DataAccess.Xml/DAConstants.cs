﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HourRegistration.DataAccess.Xml
{
    public static class DAConstants
    {
        public const string XmlDataFolder = "XmlDataFolder";
        public const string ProjectFile = "Projects.xml";
        public const string ActivityFile = "ProjectActivities.xml";
        public const string TaskFile = "IndirectTasks.xml";
    }
}

﻿using HourRegistration.DataAccess.Int;
using HourRegistration.DataEntities;
using HourRegistration.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace HourRegistration.DataAccess.Xml
{
    public class IndirectTaskDataAccessor : IDataAccessor<IndirectTask>
    {
        private IAppSettings settings;

        public IndirectTaskDataAccessor(IAppSettings appSettings)
        {
            this.settings = appSettings;
        }

        public List<IndirectTask> RetrieveEntityListByCode(string code)
        {
            throw new NotImplementedException();
        }

        public List<IndirectTask> RetrieveListOfEntities()
        {
            string fileName = this.DetermineDataFile();
            var taskList = this.RetrieveDataFromFile<List<IndirectTask>>(fileName);
            return taskList;
        }

        public IndirectTask RetrieveSingleEntity(string code)
        {
            int taskCode = 0;
            IndirectTask singleTask = null;
            if (int.TryParse(code, out taskCode))
            {
                singleTask = (from task in this.RetrieveListOfEntities()
                              where task.Task == taskCode
                              select task).FirstOrDefault();
            }

            return singleTask;
        }

        public IndirectTask RetrieveSingleEntityByParentCode(string parentCode, string code)
        {
            throw new NotImplementedException();
        }

        protected virtual string DetermineDataFile()
        {
            string dataFolder = this.settings.XmlDataFolder;
            return Path.Combine(dataFolder, DAConstants.TaskFile);
        }

        protected virtual T RetrieveDataFromFile<T>(string fileName)
        {
            return DataSerialization.DeserializeFromFile<T>(fileName);
        }
    }
}

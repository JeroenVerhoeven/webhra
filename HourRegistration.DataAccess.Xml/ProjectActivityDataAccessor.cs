﻿using HourRegistration.DataAccess.Int;
using HourRegistration.DataEntities;
using HourRegistration.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace HourRegistration.DataAccess.Xml
{
    public class ProjectActivityDataAccessor : IDataAccessor<ProjectActivity>
    {
        private IAppSettings settings;

        public ProjectActivityDataAccessor(IAppSettings appSettings)
        {
            this.settings = appSettings;
        }

        public List<ProjectActivity> RetrieveEntityListByCode(string code)
        {
            var activitiesByProject = (from activity in this.RetrieveListOfEntities()
                                       where activity.ProjectCode.Equals(code, StringComparison.InvariantCultureIgnoreCase)
                                       select activity).ToList();

            return activitiesByProject;
        }

        public List<ProjectActivity> RetrieveListOfEntities()
        {
            string fileName = this.DetermineDataFile();
            var activityList = this.RetrieveDataFromFile<List<ProjectActivity>>(fileName);
            return activityList;
        }

        public ProjectActivity RetrieveSingleEntity(string code)
        {
            throw new NotImplementedException();
        }

        public ProjectActivity RetrieveSingleEntityByParentCode(string parentCode, string code)
        {
            var activity = (from act in this.RetrieveEntityListByCode(parentCode)
                            where act.ActivityCode.Equals(code, StringComparison.InvariantCultureIgnoreCase)
                            select act).FirstOrDefault();

            return activity;
        }

        protected virtual string DetermineDataFile()
        {
            string dataFolder = this.settings.XmlDataFolder;
            return Path.Combine(dataFolder, DAConstants.ActivityFile);
        }

        protected virtual T RetrieveDataFromFile<T>(string fileName)
        {
            return DataSerialization.DeserializeFromFile<T>(fileName);
        }

    }
}

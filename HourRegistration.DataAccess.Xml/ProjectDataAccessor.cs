﻿using HourRegistration.DataAccess.Int;
using HourRegistration.DataEntities;
using HourRegistration.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace HourRegistration.DataAccess.Xml
{
    public class ProjectDataAccessor : IDataAccessor<Project>
    {
        private IAppSettings settings;

        public ProjectDataAccessor(IAppSettings appSettings)
        {
            this.settings = appSettings;
        }

        public virtual List<Project> RetrieveEntityListByCode(string code)
        {
            throw new NotImplementedException();
        }

        public virtual List<Project> RetrieveListOfEntities()
        {
            string projectsFileName = this.DetermineDataFile();
            List<Project> projects = this.RetrieveDataFromFile<List<Project>>(projectsFileName);
            return projects;
        }

        public virtual Project RetrieveSingleEntity(string code)
        {
            List<Project> projects = this.RetrieveListOfEntities();
            Project project = (from projectDE in projects
                                 where projectDE.Code.Equals(code, StringComparison.InvariantCultureIgnoreCase)
                                 select projectDE).FirstOrDefault();

            return project;
        }

        public virtual Project RetrieveSingleEntityByParentCode(string parentCode, string code)
        {
            throw new NotImplementedException();
        }

        protected virtual string DetermineDataFile()
        {
            string dataFolder = this.settings.XmlDataFolder;
            return Path.Combine(dataFolder, DAConstants.ProjectFile);
        }

        protected virtual T RetrieveDataFromFile<T>(string fileName)
        {
            return DataSerialization.DeserializeFromFile<T>(fileName);
        }
    }
}
